# Contributing

This project is pretty much feature complete. It is intended to be a mostly
minimal task tracking system for me. I do not require a large number of
features and rarely change the application. However, I do understand that other
people may have different needs.

Despite the fact that I feel the application covers my needs, I do understand
that in a few cases someone might have an idea that I might find indispensable
once I know of it. If you have this idea, I would be interested in hearing
about it. The best way to get an idea into this project is to [submit an
issue](https://gitlab.com/gwadej/timelog-rust/-/issues).

If the idea is something that fits the core goals of the program, I'd love to
collaborate on getting it included.  I will warn you, I am an old, crotchety
programmer and I may not take to your idea with the enthusiasm you expect.

If an idea doesn't fit the core goals of the program, feel free to clone the
application and make your own changes. (Keep the License in mind, however.) If
you make a new version with your whiz-bang feature let me know, I'd like to
link to other variations.

## Mechanics of Contributing

If we agree that your idea really needs to be added to `timelog`, the process
is pretty standard.

* Fork the repo on GitLab
* Clone the project to your own machine
* Commit changes to your own branch
* Push your work back up to your fork
* Submit a Pull request so that we can review your changes

_NOTE_: Be sure to merge the latest from "upstream" before making a pull request!

## Programming Style

I'm not really interested in making style changes to the code, no matter how
well-reasoned an argument you make. My personal style is close to Rust standard
but not exactly. It has evolved over ~30 years of development and won't change
without a lot of thought on my part.

If you make a change to an existing file, please follow the style of that file or
have your PR be rejected. If you add new code, it should follow either my style or
standard Rust style.

## Pull Requests

Commit messages should explain the change and be clear. Concise explanatory
detail is appreciated, but not required. Please rebase or squash commits into a
small number of logical commits. _Small_ in this context depends on the
complexity of the change.

In order to be accepted, any PR must contain tests to verify functionality. Remember
that good tests should also document functionality, not just verify the code.

On adding any externally facing functionality, the PR should also include help
for any new commands and addition to the [Manual.md] and possibly
[Tutorial.md].

## Conclusion

While I probably won't accept all ideas that come my way (based on other projects
that I have released), I would sincerely hope to remain open minded and accepting
of other opinions. Likewise, if you disagree with my opinion feel free to modify
your own fork.
