## NAME

Manual - Overview of the `rtimelog` application.

## VERSION

This manual applies to rtimelog version 0.20.0.

## INTRODUCTION

In a job a long time ago, I found that I would regularly reach the end of a
long, tiring day and have no idea what I had done. Just as regularly, I found
that my most important projects were not progressing very rapidly. Many people
find themselves in the same position. I decided to do something about it, I
found an application on the Palm that tracked Clients, Projects, and Tasks (as
well as mileage and expenses) called TEAK.

I started tracking everything I did at work. After a few days, I had identified
many of the interruptions that prevented me from making progress. I had also
noted that some of my busiest, yet least productive days were almost all
interruptions. I used this information to improve my work and to keep people
updated with why my various projects were not progressing as expected. When I
needed to make faster progress, I had the data to help my manager redirect the
worst of the interruptions.

### The Need for Change

This was wonderful. Unfortunately, the Palm platform did not survive in the
market. I continued to keep my Palm functional partially to use TEAK. But, I
eventually had to admit that I could not rely on my Palm to continue. By this
time I had an Android smart phone and I figured it would be pretty easy to find
something to replace TEAK. No such luck.

I had been using Gina Trapani's Todo.txt program for a while at this point.
It's main feature is the simplicity of its file format:

- One entry per line
- pure text
- minimal formatting

I decided that this would be a better basis for a new time logging program than
some binary format. So I wrote a quickie Perl script to allow me to start and
stop task timers and to generate simple reports.

As I used the program, I found other commands and reports that could be useful.
In the end, it was obvious that this tool needed to be cleaned up a re-written.
The Perl module version was the result of that rewrite.

The current version is a Rust port of the Perl module version.

## The `rtimelog` Program

The `rtimelog` program is how you manipulate your time log and generate
reports. The program is executed with a command and optional arguments.

## Supported Commands

The `rtimelog` program supports a number of commands that manipulate or report
on the time logged by the program.

### Adding Events

The commands you will use the most involve adding events to the time log. The
string `{event description}` is a text string that may have a _+project_ and/or
a _@task_. The first string of non-whitespace characters beginning with a '`+`'
is treated as the _project_ for this event. The project is only relevant when
using the reporting commands. Likewise, the first string of non-whitespace
characters beginning with a '`@`' is treated as a task.

If an event has no task, the non-project part of the event is treated as the
task. Otherwise, the non-project, non-task part of the event is treated as
detail information for the event.

- start {event description}

    Stop the current event and start timing a new event. Since `rtimelog` only
    tracks one event at a time, the current event is always stopped when starting
    a new event.

- stop

    Stop timing the current event.

- push {event description}

    Save the current event on stack and start timing new event. The current event
    is saved in the stack file in the timelog directory. You can manipulate the
    saved event with **resume**/**pop** or **drop**.

- resume

    Stop the current event and restart top event on stack. This also removes the
    event from the stack.

- pop

    Alias for the **resume** command.

- pause

    Save the current event on the stack and stop that event from timing. It's the
    equivalent of pushing the current event, while stopping timing at the same time.

- drop \[all|{n}\]

    Drop one or more events from top of event stack. If no arguments are supplied,
    only the top item is dropped. If the argument is a number, that number of
    events is dropped from the stack. If the argument _all_ is supplied, the stack
    is cleared.

### Reporting

The reporting commands generate a report of information from the timelog files.
Many of these commands accept a _date_ or pair of _date_ s. The date is
normally expected to be of the form `YYYY-MM-DD`. However, a small number
of shortcuts are also allowed.

The strings '_today_' and '_yesterday_' resolve to the obvious date. The days
of the week are also allowed, each resolves to that day of the week prior to the
current day. If the current day is Wednesday, _monday_ resolves to two days ago,
and _thursday_ resolves to six days ago.

Any time you supply a pair of dates, the second must be later in time than the
first.

The reporting commands are:

- hours \[date \[end date\]\]

    Display the hours worked for each of the appropriate days.

- ls \[date\]

    List events for the specified day. Default to today.

- lsproj

    List known projects, one per line.

- lstk

    Display items on the stack. The top event on the stack is the one that is
    resumed by the **pop** command.

- report \[date \[end date\]\]

    Display a report for the specified days.

    This report is grouped by day, with each project grouped under the day.
    Each event for a project is displayed under the project. Accumulated times are
    displayed for each project and day.

- summary \[date \[end date\]\]

    Display a summary of the appropriate days' projects.

    For each day, the list of projects active during that day is shown along with
    the aggregate time spent on that project.

- chart \[date \[end date\]\]

    Create a graphical dashboard containing pie charts showing the proportion
    of projects during the day, and tasks for each project, as well as bar graph
    showing how time during the day is distributed into projects.

- curr

    Display the start date and time and event description of the event that is
    currently being timed, if any.

### Other Commands

The remaining commands do not necessarily fit into one of the other categories.

- init \[dir\]

    Prepare the system for use with `rtimelog`. First, it creates the directory
    where rtimelog stores its information, if that directory does not already exist.
    Then, the command creates and initializes the `.timelogrc` configuration file,
    in the user's home directory, with the information needed to run.

    If a directory is not supplied, use the default location of `timelog` in
    the user's home directory.

- edit

    Open the timelog file in the current editor. The editor can be specified in the
    configuration file. If no editor is specified in the configuration, the program
    uses the value of the `VISUAL` environment variable. If `VISUAL` has no value,
    then the value of `EDITOR` is used instead. If neither environment variable has
    a value, then the program defaults to `vim`

- archive

    Move all entries from the earliest year in the logfile, into a new file in the
    timelog directory named `timelog-{year}.txt`. No entries are moved from the
    current year.

- help \[command\]

    Display help about commands. The _help_ command gives more detailed help
    about the command.

    With no arguments, you get a list of the commands, each with its associated
    help text. The commands are each listed as a usage line with some
    explanatory text.

    Calling _help_ with an argument of '_commands_' lists the help information
    for just the commands.

    Any other argument is looked up in the command list.

## Configuration

The `rtimelog` program uses the file `~/.timelogrc` if it exists.

The configuration file is expected to contain data in two major parts:

### General Configuration

The first section defined general configuration information in a key=value
format. The recognized keys are:

- editor

    The editor to use when opening the timelog file with the `edit` command.
    If not specified, it will use the value of either the VISUAL or EDITOR
    environment variables. If none are found, it will default to `vim`.

- browser

    The browser command to use when displaying the report generated by the
    `chart` command. If not specified, it will use a hard-coded value of
    `chromium-browser`, except on Mac OS, where it will use `open`.

- dir

    The directory in which to find the timelog data files. Defaults to the
    `timelog` directory in the user's home directory.

- defcmd

    The default command to by used if none is supplied to timelog. By default,
    this is the '**stop**' command.

### Command Aliases

The configuration file may also contain an '`[alias]`' section that defines
command aliases. Each alias is defined as a `shortname=expanded string`.

For example, if you regularly need to make entries for reading email and
triaging bug reports you might want the following in your configuration.

    [alias]
      email = start +Misc @Email
      triage = start +BugTracker @Triage

## AUTHOR

G. Wade Johnson  `<rust@gwadej.org>`

## LICENCE AND COPYRIGHT

Copyright (c) 2021-2022, G. Wade Johnson `<rust@gwadej.org>`. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See [perlartistic](https://opensource.org/licenses/Artistic-2.0).
