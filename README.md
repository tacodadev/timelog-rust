# TimeLog

Version of the rtimelog program written in Rust.

## Purpose

The rtimelog program provides a low-impact, simple tool for tracking a person's
time. It tracks individual changes in task _events_ in a simple text file.

Rtimelog is a command line tool that supports adding and ending events. It also
supports different reports on the amount of time spend on various tasks.

Full documentation of the tool is found in [Manual.md](Manual.md) and [Tutorial.md](Tutorial.md).

The file [Format.md](Format.md) provides information about how events are stored in the timelog
file. This would be useful if you want to modify the file or generate your own
reports from the file.

## Badges

![I'm working on it](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg "maintenace badge")
