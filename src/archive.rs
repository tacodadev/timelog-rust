//! Handle the archiving of old records from the timelog.

use std::fs::{rename, File, OpenOptions};
use std::io::prelude::*;
use std::io::{BufRead, BufReader, BufWriter};
use std::path::Path;

use lazy_static::lazy_static;
use regex::Regex;

use crate::config::Config;
#[doc(inline)]
use crate::date::Date;
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;
#[doc(inline)]
use crate::logfile::Logfile;
use crate::Result;

lazy_static! {
    /// Regular expression matching the year portion of an event line.
    static ref YEAR_RE: Regex = Regex::new(r"^(\d\d\d\d)").expect("Date regex failed");
}

/// Configuration for archiving previous year information from the timelog.txt file.
pub(crate) struct Archiver<'a> {
    // Reference to the rtimelog configuration.
    config: &'a Config,
    // The current year
    curr_year: u32,
    // The name for the new file created to store the previous year.
    new_file: String,
    // The name for the backup file.
    back_file: String,
}

impl<'a> Archiver<'a> {
    /// Create a new Archiver object.
    pub(crate) fn new(config: &'a Config) -> Self {
        Self {
            config,
            curr_year: Date::today().year() as u32,
            new_file: format!("{}.new", config.logfile()),
            back_file: format!("{}.bak", config.logfile()),
        }
    }

    // Return the [`Logfile`] object representing the file on disk
    //
    // ## Errors
    //
    // - Return [`PathError::FilenameMissing`] if the `file` has no filename.
    // - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
    fn logfile(&self) -> Result<Logfile> {
        Ok(Logfile::new(&self.config.logfile())?)
    }

    // Return the year for the supplied event line.
    //
    // ## Errors
    //
    // - Return [`Error::InvalidEventLine`] if the event line can't be parsed.
    fn extract_year(&self, line: &str) -> Result<u32> {
        YEAR_RE
            .captures(line)
            .map(|cap| cap[0].parse::<u32>().unwrap())
            .ok_or(Error::InvalidEventLine)
    }

    // Return an appropriate path/filename for the supplied year.
    fn archive_filepath(&self, year: u32) -> String {
        format!("{}/timelog-{}.txt", self.config.dir(), year)
    }

    // Return a [`BufWriter`] wrapping the file for writing to the supplied filename.
    //
    // ## Errors
    //
    // - Return [`PathError::FileAccess`] if unable to open the file.
    fn archive_writer(filename: &str) -> Result<BufWriter<File>> {
        let file = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(&filename)
            .map_err(|e| PathError::FileAccess(filename.to_owned(), e.to_string()))?;
        Ok(BufWriter::new(file))
    }

    /// Archive the first (non-current) year from the logfile.
    ///
    /// - Return Ok(Some(year)) if `year` was archived.
    /// - Return Ok(None) if no previous year to archive.
    ///
    /// ## Errors
    ///
    /// - Return [`Error::PathError`] for any error accessing the log or archive files.
    pub(crate) fn archive(&self) -> Result<Option<u32>> {
        let file = self.logfile()?.open()?;
        let mut lines = BufReader::new(file).lines();
        let first = match lines.next() {
            Some(Ok(line)) => line,
            _ => return Ok(None),
        };
        let arc_year = self.extract_year(&first)?;
        if arc_year >= self.curr_year {
            return Ok(None);
        }

        let logfile = self.config.logfile();
        let archive_filename = self.archive_filepath(arc_year);
        if Path::new(&archive_filename).exists() {
            return Err(Error::from(PathError::AlreadyExists(archive_filename)));
        }
        let mut arc_stream = Self::archive_writer(&archive_filename)?;
        let mut new_stream = Self::archive_writer(&self.new_file)?;

        writeln!(&mut arc_stream, "{}", first)
            .map_err(|e| PathError::FileWrite(archive_filename.clone(), e.to_string()))?;

        for line in lines {
            let ln = line.map_err(|e| PathError::FileAccess(logfile.clone(), e.to_string()))?;
            let mut stream = (self.extract_year(&ln)? == arc_year)
                .then(|| &mut arc_stream)
                .unwrap_or(&mut new_stream);

            writeln!(&mut stream, "{}", ln)
                .map_err(|e| PathError::FileWrite(archive_filename.clone(), e.to_string()))?;
        }

        Self::flush(&archive_filename, &mut arc_stream)?;
        Self::flush(&self.new_file, &mut new_stream)?;
        Self::rename(&logfile, &self.back_file)?;
        Self::rename(&self.new_file, &logfile)?;

        Ok(Some(arc_year))
    }

    // Utility method for flushing a writer and properly reporting any error.
    fn flush(filename: &str, stream: &mut BufWriter<File>) -> Result<()> {
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(filename.to_string(), e.to_string()).into())
    }

    // Utility method for renaming a file and properly reporting any error.
    fn rename(old: &str, new: &str) -> Result<()> {
        rename(old, new)
            .map_err(|e| PathError::RenameFailure(old.to_string(), e.to_string()).into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_new() {
        let config = Config::default();
        let arch = Archiver::new(&config);
        let logfile = config.logfile();
        assert_that!(arch.config).is_equal_to(&config);
        assert_that!(arch.curr_year).is_equal_to(&(Date::today().year() as u32));
        assert_that!(arch.new_file).is_equal_to(&format!("{}.new", logfile));
        assert_that!(arch.back_file).is_equal_to(&format!("{}.bak", logfile));
    }

    #[test]
    fn test_extract_year() {
        let line = "2018-11-20 12:34:43 +test @Event";
        let config = Config::default();
        let arch = Archiver::new(&config);
        assert_that!(arch.extract_year(line))
            .is_ok()
            .is_equal_to(2018);
    }

    #[test]
    fn test_extract_year_fail() {
        let line = "xyzzy 2018-11-20 12:34:43 +test @Event";
        let config = Config::default();
        let arch = Archiver::new(&config);
        assert_that!(arch.extract_year(line)).is_err();
    }

    #[test]
    fn test_archive_filepath() {
        let config = Config::default();
        let arch = Archiver::new(&config);
        let expect = format!("{}/timelog-{}.txt", config.dir(), 2011);
        assert_that!(arch.archive_filepath(2011)).is_equal_to(&expect);
    }
}
