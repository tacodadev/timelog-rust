//! Executable handling the rtimelog actions
//!
//! See the [`timelog`] library for all of the supporting code.

extern crate structopt;

use std::process;

use structopt::StructOpt;
use timelog::Cli;

fn main() {
    let cli = Cli::from_args();

    if let Err(e) = cli.run() {
        eprintln!("{}", e);
        process::exit(1);
    }
}
