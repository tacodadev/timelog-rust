//! Represent the construction of an SVG histogram.

use crate::chart::colors::ColorMap;
use crate::chart::{DayHours, Percentages};
use crate::chart::day::Hour;
use crate::TaskEvent;
use crate::emit_xml;
use crate::Result;

use std::io::Write;

use xml::writer::{EventWriter, XmlEvent};

// Styles for the bar graph
const STYLESHEET: &str = r#"
text {
    font-size: 12;
    text-anchor: middle;
}
"#;

/// Configuration for drawing an hourly graph showing projects worked during
/// that hour.
pub struct BarGraph {
    bar_width: usize,
    colors: ColorMap,
}

impl BarGraph {
    /// Create a [`BarGraph`] from the supplied percentages.
    pub fn new(percents: &Percentages) -> Self {
        Self { bar_width: 20, colors: ColorMap::new(percents) }
    }

    // Draw a single bar for the supplied [`Hour`].
    fn write_hour<W: Write>(&self, w: &mut EventWriter<W>, begin: usize, hr: &Hour, i: usize) -> Result<()> {
        let id = format!("hr{:02}", i + begin);
        let xform = format!("translate({}, 60)", i * self.bar_width);
        emit_xml!(w, g, id: &id, transform: &xform => {
            let mut offset = 0.0;
            for task in hr.iter() {
                offset = self.write_task(w, task, offset)?;
            }
            emit_xml!(w, text, x: "10", y: "13"; &format!("{}", begin + i))
        })
    }

    // Draw the block for a single [`TaskEvent`] on the current hour.
    fn write_task<W: Write>(&self, w: &mut EventWriter<W>, task: &TaskEvent, offset: f32) -> Result<f32> {
        let height = task.as_secs() as f32 / 60.0;
        let offset = offset - height;
        let ht_str = format!("{:.3}", height);
        let off_str = format!("{:.3}", offset);
        let bar = format!("{}", self.bar_width - 1);
        emit_xml!(w, rect, x: "0", y: &off_str, height: &ht_str, width: &bar,
                fill: self.colors.get(&task.project()).unwrap())?;
        Ok(offset)
    }

    /// Write an SVG representation of the [`DayHours`] as a bar graph.
    pub fn write<W: Write>(&self,  w: &mut EventWriter<W>, day_hours: &DayHours) -> Result<()> {
        let width = format!("{}", day_hours.num_hours() * self.bar_width);
        let view = format!("0 0 {} 80", width);
        emit_xml!(w, svg, view: &view, width: &width, height: "80" => {
            emit_xml!(w, style; STYLESHEET)?;
            let path = format!("M0,60 h{}", day_hours.num_hours() * self.bar_width - 1);
            emit_xml!(w, path, d: &path, stroke: "black")?;

            let begin = day_hours.start();
            for (i, hr) in day_hours.iter().enumerate() {
                self.write_hour(w, begin, &hr, i)?;
            }
            Ok(())
        })
    }
}
