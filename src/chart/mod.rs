//! Representation of charts for timelog
//!
//! # Description
//!
//! Support for both pie charts (of projects and tasks in a project), and
//! a bar graph of tasks displayed hourly.

pub mod colors;
pub mod day;
pub mod histogram;
pub mod legend;
pub mod pie;
pub mod pie_data;
pub mod tag_percent;

/// The [`ColorIter`] type represents an ordered list of colors to use for
/// building a pie chart.
pub type ColorIter<'a> = colors::ColorIter<'a>;

/// The [`TagPercent`] type holds a labeled percent value used in constructing
/// pie charts.
pub type TagPercent = tag_percent::TagPercent;

/// The [`PieData`] type holds the data for a pie chart.
pub type PieData = pie_data::PieData;

/// The [`Legend`] type formats the labels for a legend on the pie chart.
pub type Legend<'a> = legend::Legend<'a>;

/// The [`PieChart`] type holds the configuration data needed to display a
/// pie chart.
pub type PieChart<'a> = pie::PieChart<'a>;

/// The [`Percentages`] type is a vector of [`TagPercent`]s.
pub type Percentages = Vec<TagPercent>;

/// The [`BarGraph`] type holds configuration data needed to display an
/// hourly bar graph of the projects worked.
pub type BarGraph = histogram::BarGraph;

/// The [`DayHours`] type holds project-based task events for building an
/// hourly bar graph of the projects worked.
pub type DayHours = day::DayHours;
