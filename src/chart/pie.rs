//! Represent the construction of an SVG PieChart.
//!
//! # Examples
//!
//! ```rust, no_run
//! use std::fs::File;
//! use xml::{EventWriter, EmitterConfig};
//! use timelog::chart::{ColorIter, Legend, Percentages, PieChart, PieData};
//! # use std::fmt;
//!
//! # fn main() {
//! let mut piedata = PieData::new();
//! [("foo", 10), ("bar", 70), ("baz", 50), ("foobar", 35)].iter()
//!     .for_each(|(l, p)| piedata.add_secs(l, *p));
//! let percents: Percentages = piedata.percentages();
//! let radius = 100.0;
//! let mut pie = PieChart::new(
//!     radius,
//!     Legend::new(14.0, ColorIter::default()),
//! );
//! let mut file = File::create("output.svg").expect("Can't create file");
//! let mut w = EmitterConfig::new().create_writer(&mut file);
//! pie.write_pie(&mut w, &percents);
//! # }
//! ```

use std::io::Write;

use xml::writer::{EventWriter, XmlEvent};

use crate::chart::legend::Legend;
use crate::chart::tag_percent::TagPercent;

use crate::chart::colors::ColorIter;
use crate::emit_xml;
use crate::Result;

/// Configuration of a pie chart.
pub struct PieChart<'a> {
    r: f32,
    legend: Legend<'a>
}

impl<'a> PieChart<'a> {
    /// Create a new [`PieChart`] with the given radius and [`Legend`]
    pub fn new(r: f32, legend: Legend<'a>) -> Self {
        Self {
            r,
            legend,
        }
    }

    /// Output a pie chart representing the supplied percentages.
    pub fn write_pie<W: Write>(&self, w: &mut EventWriter<W>, percents: &[TagPercent]) -> Result<()> {
        emit_xml!(w, div, class: "piechart" => {
            emit_xml!(w, div, class: "pie" => {
                let size = format!("{}", 2.0 * (self.r + 1.0));
                let view = format!("{0:} {0:} {1:} {1:}", -(self.r + 1.0), size);
                emit_xml!(w, svg, viewbox: &view, width: &size, height: &size => {
                    emit_xml!(w, circle, r: &format!("{}", self.r), fill: "black")?;
                    let colors = ColorIter::default();
                    let percents = colors.limit_percents(percents, "Other");
                    let mut alpha = -90.0;
                    for (p, clr) in percents.iter().zip(colors) {
                        let theta = p.percent() * 3.6f32;

                        emit_xml!(w, path, fill: clr, d: &self.pie_slice(alpha, theta))?;
                        alpha += theta;
                    }
                    Ok(())
                })
            })?;
            self.legend.write(w, percents.iter())?;
            Ok(())
        })
    }

    // Create a single pie slice between the angles of `alpha` and `theta`.
    fn pie_slice(&self, alpha: f32, theta: f32) -> String {
        let ralpha = alpha.to_radians();
        let sx = self.r * ralpha.cos();
        let sy = self.r * ralpha.sin();

        if theta == 360.0 {
            let rend = (alpha + 180.0).to_radians();
            let ex = self.r * rend.cos();
            let ey = self.r * rend.sin();

            format!("M0,0 L{sx:.3},{sy:.3} A{r},{r} 0 1,1 {ex:.3},{ey:.3} A{r},{r} 0 1,1 {sx:.3},{sy:.3} z",
                    sx=sx, sy=sy,
                    r=self.r,
                    ex=ex, ey=ey
            )
        }
        else {
            let rend = (alpha + theta).to_radians();
            let ex = self.r * rend.cos();
            let ey = self.r * rend.sin();

            let large = if theta < 180.0 { 0 } else { 1 };
            format!("M0,0 L{sx:.3},{sy:.3} A{r},{r} 0 {lg},1 {ex:.3},{ey:.3} z",
                    sx=sx, sy=sy,
                    r=self.r,
                    lg=large,
                    ex=ex, ey=ey
            )
        }
    }
}
