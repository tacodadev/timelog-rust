//! The [`PieData`] type represents the data in a pie chart.
//!
//! # Examples
//!
//! # Description
//!
//! The [`PieData`] holds the data used to construct a pie chart. It supports
//! adding data to build up the data set.

use std::cmp::{Ordering, PartialOrd};
use std::collections::HashMap;

#[doc(inline)]
use crate::chart::{Percentages, TagPercent};

/// Representation of the data used to construct the pie chart.
#[derive(Debug)]
pub struct PieData {
    total: f32,
    data: HashMap<String, f32>
}

impl PieData {
    /// Create a brand-new [`PieData`] object with no data.
    pub fn new() -> Self {
        Self { total: 0.0, data: HashMap::new() }
    }

    /// Add the supplied number of seconds to the supplied label.
    pub fn add_secs(&mut self, label: &str, secs: u64) {
        self.total += secs as f32;
        let entry = self.data.entry(label.to_string()).or_insert(0.0);
        *entry += secs as f32;
    }

    /// Return the percentage associated with the supplied label, if any.
    pub fn get_percent(&self, label: &str) -> Option<f32> {
        self.data.get(label).map(|&s| self.cent(s))
    }

    /// Return `true` if no data has been added to the [`PieData`]
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    /// Return the total sum of the values in the data
    pub fn total(&self) -> f32 {
        self.total
    }

    /// Return a sorted list of the labels in the [`PieData`].
    pub fn labels(&self) -> Vec<&String> {
        let mut labels: Vec<&String> = self.data.keys().collect();
        labels.sort();
        labels
    }

    /// Return list of [`TagPercent`] objects sorted by descending percentage
    /// and ascending label.
    pub fn percentages(&self) -> Percentages {
        let mut percents: Percentages =
            self.data
                .iter()
                .filter_map(|(l, p)| TagPercent::new(l, self.cent(*p)))
                .collect();

        percents.sort_by(|a, b| a.partial_cmp(&b).unwrap_or(Ordering::Equal));
        percents
    }

    // Utility method for converting a stored value into a percentage.
    fn cent(&self, val: f32) -> f32 {
        100.0 * val / self.total
    }
}
