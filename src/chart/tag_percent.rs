//! Data struct representing a label and percentage.
//!
//! # Examples
//!
//! ```rust
//! use timelog::chart::TagPercent;
//!
//! # fn main() {
//! let tp = TagPercent::new("One tenth", 10.0).unwrap();
//!
//! if tp.percent() < 50.0 {
//!     println!("'{}' is less than half", tp.label());
//! }
//!
//! println!("{}", tp.display_label());
//! # }
//! ```

use std::cmp::{Ordering, PartialOrd};

/// Structure holding a percent value with a label
#[derive(Debug, Clone)]
pub struct TagPercent {
    percent: f32,
    label: String
}

impl TagPercent {
    /// Create a new [`TagPercent`] object if the percentage is between 0 and 100
    /// inclusive, otherwise `None`.
    pub fn new(label: &str, percent: f32) -> Option<Self> {
        if percent <= 0.0 || percent > 100.0 {
            return None;
        }
        Some(Self { percent, label: label.to_owned() })
    }

    /// Format the supplied percent as a [`String`], assuming a positive floating point number
    /// less than or equal to 100.0. If you use a number outside that range, the formatting
    /// may not be appropriate.
    ///
    /// - Numbers less than 1 will be formatted with one decimal place.
    /// - Numbers greater than or equal to 1 will be formatted as whole numbers.
    pub fn fmt_percent(percent: f32) -> String {
        if percent < 1.0 {
            format!("{:.1}%", percent)
        }
        else {
            format!("{:.0}%", percent)
        }
    }

    /// Return a reference to the label.
    pub fn label(&self) -> &str {
        &self.label
    }

    /// Return a display label
    pub fn display_label(&self) -> String {
        format!("{} - {}", Self::fmt_percent(self.percent), self.label)
    }

    /// Return the percentage value.
    pub fn percent(&self) -> f32 {
        self.percent
    }
}

impl PartialOrd for TagPercent {
    /// Compare two [`TagPercent`] values.
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let ord = other.percent.partial_cmp(&self.percent);
        match ord {
            Some(Ordering::Equal) => self.label.partial_cmp(&other.label),
            _ => ord
        }
    }
}

impl PartialEq for TagPercent {
    /// Return `true` if two [`TagPercent`] values are equal.
    fn eq(&self, other: &Self) -> bool {
        (self.percent == other.percent)
            && (self.label == other.label)
    }
}

impl Eq for TagPercent {}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_new() {
        assert_that!(TagPercent::new("foo", 50.0))
            .contains(TagPercent { percent: 50.0, label: "foo".to_string() });
    }

    #[test]
    fn test_display_label() {
        assert_that!(TagPercent::new("foo", 100.0).unwrap().display_label())
            .is_equal_to("100% - foo".to_string());
        assert_that!(TagPercent::new("foo", 50.0).unwrap().display_label())
            .is_equal_to("50% - foo".to_string());
        assert_that!(TagPercent::new("foo", 4.0).unwrap().display_label())
            .is_equal_to("4% - foo".to_string());
    }

    #[test]
    fn test_new_zero() {
        assert_that!(TagPercent::new("foo", 0.0)).is_none();
    }

    #[test]
    fn test_new_negative() {
        assert_that!(TagPercent::new("foo", -10.0)).is_none();
    }

    #[test]
    fn test_new_too_large() {
        assert_that!(TagPercent::new("foo", 100.1)).is_none();
    }

    #[test]
    fn test_accessors() {
        let val = TagPercent::new("bar", 37.5).unwrap();
        assert_that!(val.label()).is_equal_to("bar");
        assert_that!(val.percent()).is_equal_to(37.5);
    }

    #[test]
    fn test_order_by_percentage_same_label() {
        let val1 = TagPercent::new("bar", 10.0).unwrap();
        let val2 = TagPercent::new("bar", 15.0).unwrap();
        assert_that!(val1.partial_cmp(&val2))
            .is_some()
            .is_equal_to(Ordering::Greater);
        assert_that!(val2.partial_cmp(&val1))
            .is_some()
            .is_equal_to(Ordering::Less);
    }

    #[test]
    fn test_order_by_percentage_diff_label() {
        let val1 = TagPercent::new("bar", 10.0).unwrap();
        let val2 = TagPercent::new("foo", 15.0).unwrap();
        assert_that!(val1.partial_cmp(&val2))
            .is_some()
            .is_equal_to(Ordering::Greater);
        assert_that!(val2.partial_cmp(&val1))
            .is_some()
            .is_equal_to(Ordering::Less);
    }

    #[test]
    fn test_order_by_label_same_percentage() {
        let val1 = TagPercent::new("bar", 10.0).unwrap();
        let val2 = TagPercent::new("foo", 10.0).unwrap();
        assert_that!(val1.partial_cmp(&val2))
            .is_some()
            .is_equal_to(Ordering::Less);
        assert_that!(val2.partial_cmp(&val1))
            .is_some()
            .is_equal_to(Ordering::Greater);
    }
}
