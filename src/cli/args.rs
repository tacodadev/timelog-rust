//! Structs defining different sets of arguments supplied on command line
//!
//! # Description
//!
//! - [`DateRangeArgs`] - representation of the start/end dates for a report.
//! - [`DropArg`] - represents the arguments passed to the `drop` command.
//! - [`FilterArgs`] - representation of the start/end dates and project list for reports.
use std::iter::once;

use regex::Regex;

#[doc(inline)]
use crate::date::Date;
use crate::Day;
#[doc(inline)]
use crate::error::Error;
use crate::Result;

/// Represent the two styles of drop arguments. The string 'all' or the number of items to drop.
#[derive(Debug, PartialEq)]
pub enum DropArg {
    All,
    Num(u32),
}

impl DropArg {
    /// Parse the optional string into a [`DropArg`]
    pub fn parse(arg: &Option<String>) -> Result<Self> {
        match arg.as_ref().map(|a| a.as_str()) {
            Some("all") => Ok(Self::All),
            Some("0") => Err(Error::InvalidDrop("0".to_owned())),
            Some(num) => Ok(Self::Num(
                num.parse()
                    .map_err(|_| Error::InvalidDrop(num.to_owned()))?,
            )),
            None => Ok(Self::Num(1)),
        }
    }
}

/// Trait specifying common functionality for the different filter arguments.
pub trait DayFilter {
    /// Return the start date as a [`String`]
    fn start(&self) -> String;
    /// Return the end date as a [`String`]
    fn end(&self) -> String;
    /// Return a [`Day`] object after appropriate filtering
    fn filter_day(&self, day: Day) -> Option<Day>;
}

// Return Some [`Day`] if the supplied day is not empty.
fn day_with_entries(day: Day) -> Option<Day> {
    (!day.is_empty()).then(|| day)
}

/// Representation of the start/end date and project list arguments for reports.
#[derive(Debug)]
pub struct FilterArgs {
    start: Date,
    end: Date,
    projects: Option<Regex>,
}

// Return a [`Regex`] one of the supplied projects
fn regex_from_projs(projs: &[&str]) -> Result<Regex> {
    Regex::new(&projs.join("|")).map_err(|_| Error::BadProjectFilter)
}

impl FilterArgs {
    /// Create the [`FilterArgs`] from an array of strings.
    ///
    /// ## Errors
    ///
    /// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
    /// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
    pub fn new(args: &[String]) -> Result<Self> {
        let mut project_list: Vec<&str> = Vec::new();
        let today = Date::parse("today")?;

        let mut arg_iter = args.iter();
        let start = match arg_iter.next() {
            None => return Ok(Self::from_date(today)),
            Some(arg) => (Date::parse(arg), arg),
        };
        let start = match start {
            (Ok(date), _) => date,
            (Err(_), arg) => {
                once(arg)
                    .chain(arg_iter)
                    .for_each(|arg| project_list.push(arg));
                return Ok(Self::from_date_and_proj(today, regex_from_projs(&project_list)?));
            }
        };
        let end = match arg_iter.next() {
            None => return Ok(Self::from_date(start)),
            Some(arg) => match Date::parse(arg) {
                Ok(date) => date.succ(),
                Err(_) => {
                    project_list.push(arg);
                    start.succ()
                }
            },
        };
        arg_iter.for_each(|arg| project_list.push(arg));
        let projects = (!project_list.is_empty())
            .then(|| regex_from_projs(&project_list))
            .transpose()?;

        (start < end).then(|| Self { start, end, projects }).ok_or(Error::WrongDateOrder)
    }

    // Create a [`FilterArgs`] from a single supplied [`Date`].
    fn from_date(start: Date) -> Self {
        Self {start, end: start.succ(), projects: None}
    }

    // Create a [`FilterArgs`] from a single supplied [`Date`] and project [`Regex`].
    fn from_date_and_proj(start: Date, proj: Regex) -> Self {
        Self {start, end: start.succ(), projects: Some(proj)}
    }

    // Return an [`Option<&Regex>`] that determines how to match projects
    fn projects(&self) -> Option<&Regex> {
        self.projects.as_ref()
    }
}

impl DayFilter for FilterArgs {
    /// Return the start date as a [`String`]
    fn start(&self) -> String {
        self.start.to_string()
    }

    /// Return the end date as a [`String`]
    fn end(&self) -> String {
        self.end.to_string()
    }

    /// Return a [`Day`] object filtered as needed
    fn filter_day(&self, day: Day) -> Option<Day> {
        day_with_entries(match self.projects() {
            Some(re) => day.filtered_by_project(re),
            None => day,
        })
    }
}

/// Representation of the start and end date arguments for reports.
#[derive(Debug, PartialEq, Eq)]
pub struct DateRangeArgs {
    start: Date,
    end: Date
}

impl DateRangeArgs {
    /// Create the [`DateRangeArgs`] from an array of strings.
    ///
    /// ## Errors
    ///
    /// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
    pub fn new(args: &[String]) -> Result<Self> {
        let today = Date::parse("today")?;

        let mut arg_iter = args.iter();
        let start = match arg_iter.next() {
            None => return Ok(Self { start: today, end: today.succ() }),
            Some(arg) => Date::parse(arg),
        };
        let start = match start {
            Ok(date) => date,
            Err(_) => {
                return Ok(Self { start: today, end: today.succ() });
            }
        };
        let end = match arg_iter.next() {
            None => start.succ(),
            Some(arg) => match Date::parse(arg) {
                Ok(date) => date.succ(),
                Err(_) => start.succ(),
            },
        };

        (start < end).then(|| Self { start, end }).ok_or(Error::WrongDateOrder)
    }

    /// Return the start date as a [`String`]
    pub fn start(&self) -> String {
        self.start.to_string()
    }

    /// Return the end date as a [`String`]
    pub fn end(&self) -> String {
        self.end.to_string()
    }
}

impl DayFilter for DateRangeArgs {
    /// Return the start date as a [`String`]
    fn start(&self) -> String {
        self.start.to_string()
    }

    /// Return the end date as a [`String`]
    fn end(&self) -> String {
        self.end.to_string()
    }

    /// Return a [`Day`] object filtered as needed. If the day is empty,
    /// return None.
    fn filter_day(&self, day: Day) -> Option<Day> {
        day_with_entries(day)
    }
}

// Only used for testing, not particularly performant.
#[cfg(test)]
impl PartialEq for FilterArgs {
    fn eq(&self, other: &Self) -> bool {
        (self.start() == other.start())
            && (self.end() == other.end())
            && match (self.projects(), other.projects()) {
                (None, None) => true,
                (None, _) | (_, None) => false,
                (Some(lhs), Some(rhs)) => format!("{:?}", lhs) == format!("{:?}", rhs),
            }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    // Test DropArg

    #[test]
    fn test_drop_no_arg() {
        assert_that!(DropArg::parse(&None))
            .is_ok()
            .is_equal_to(&DropArg::Num(1));
    }

    #[test]
    fn test_drop_all() {
        let arg: Option<String> = Some("all".to_string());
        assert_that!(DropArg::parse(&arg))
            .is_ok()
            .is_equal_to(&DropArg::All);
    }

    #[test]
    fn test_drop_int() {
        let arg: Option<String> = Some("2".to_string());
        assert_that!(DropArg::parse(&arg))
            .is_ok()
            .is_equal_to(&DropArg::Num(2));
    }

    #[test]
    fn test_drop_float() {
        let arg: Option<String> = Some("3.14".to_string());
        assert_that!(DropArg::parse(&arg)).is_err();
    }

    #[test]
    fn test_drop_num_0() {
        let arg: Option<String> = Some("0".to_string());
        assert_that!(DropArg::parse(&arg)).is_err();
    }

    #[test]
    fn test_drop_num_neg() {
        let arg: Option<String> = Some("-3".to_string());
        assert_that!(DropArg::parse(&arg)).is_err();
    }

    // Test Filter

    #[test]
    fn test_filter_no_args() {
        let args = vec![];
        let expected = FilterArgs {
            start: Date::today(),
            end: Date::today().succ(),
            projects: None,
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_filter_just_one_date() {
        let args = vec!["yesterday".to_string()];
        let expected = FilterArgs {
            start: Date::today().pred(),
            end: Date::today(),
            projects: None,
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_filter_just_two_dates() {
        let args = vec!["2021-12-01".to_string(), "2021-12-07".to_string()];
        let expected = FilterArgs {
            start: Date::new(2021, 12, 1).unwrap(),
            end: Date::new(2021, 12, 8).unwrap(),
            projects: None,
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_filter_just_project() {
        let args = vec!["project1".to_string()];
        let expected = FilterArgs {
            start: Date::today(),
            end: Date::today().succ(),
            projects: Some(Regex::new(r"project1").unwrap()),
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_filter_just_multiple_projects() {
        let args = vec![
            "project1".to_string(),
            "cleanup".to_string(),
            "profit".to_string(),
        ];
        let expected = FilterArgs {
            start: Date::today(),
            end: Date::today().succ(),
            projects: Some(Regex::new(r"project1|cleanup|profit").unwrap()),
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_filter_start_and_project() {
        let args = vec!["2021-12-01".to_string(), "project1".to_string()];
        let expected = FilterArgs {
            start: Date::new(2021, 12, 1).unwrap(),
            end: Date::new(2021, 12, 2).unwrap(),
            projects: Some(Regex::new(r"project1").unwrap()),
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_filter_both_dates_and_project() {
        let args = vec![
            "2021-12-01".to_string(),
            "2021-12-07".to_string(),
            "project1".to_string(),
        ];
        let expected = FilterArgs {
            start: Date::new(2021, 12, 1).unwrap(),
            end: Date::new(2021, 12, 8).unwrap(),
            projects: Some(Regex::new(r"project1").unwrap()),
        };

        assert_that!(&FilterArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    // Test DateRange

    #[test]
    fn test_dates_no_args() {
        let args = vec![];
        let expected = DateRangeArgs {
            start: Date::today(),
            end: Date::today().succ(),
        };

        assert_that!(&DateRangeArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_dates_just_one_date() {
        let args = vec!["yesterday".to_string()];
        let expected = DateRangeArgs {
            start: Date::today().pred(),
            end: Date::today(),
        };

        assert_that!(&DateRangeArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_dates_both_dates() {
        let args = vec!["2021-12-01".to_string(), "2021-12-07".to_string()];
        let expected = DateRangeArgs {
            start: Date::new(2021, 12, 1).unwrap(),
            end: Date::new(2021, 12, 8).unwrap(),
        };

        assert_that!(&DateRangeArgs::new(&args))
            .is_ok()
            .is_equal_to(&expected);
    }
}
