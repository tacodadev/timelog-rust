//! Implementations of the commands run by the CLI

use std::collections::HashSet;
use std::fs::{self, OpenOptions};
use std::io::{BufRead, BufReader};
use std::process::{Command, Stdio};

use xml::writer::{EmitterConfig, XmlEvent};

use lazy_static::lazy_static;
use regex::Regex;

#[doc(inline)]
use crate::archive::Archiver;
use crate::config::Config;
#[doc(inline)]
use crate::date::Date;
#[doc(inline)]
use crate::date::DateTime;
use crate::day::format_dur;
#[doc(inline)]
use crate::day::Day;
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;
#[doc(inline)]
use crate::event::Event;
#[doc(inline)]
use crate::logfile::Logfile;
#[doc(inline)]
use crate::stack::Stack;
#[doc(inline)]
use crate::task_line_iter::TaskLineIter;
use crate::Result;
use crate::emit_xml;

#[doc(inline)]
use crate::cli::args::DropArg;
#[doc(inline)]
use crate::cli::args::FilterArgs;
#[doc(inline)]
use crate::cli::args::DateRangeArgs;

use crate::cli::args::DayFilter;

lazy_static! {
    /// Regular expression matching a project.
    static ref PROJECT_RE: Regex = Regex::new(r"\+(\S+)").expect("Event project regex failed.");
}

/// The style definition for the chart HTML.
const STYLESHEET: &str = r#"
    .day {
      display: grid;
      grid-template-columns: 25% 1fr;
      grid-template-rows: 4em auto;
      padding-left: 0.5em;
      padding-bottom: 2em;
    }
    .day:nth-child(even) { background-color: #eee; }
    .day + .day {
      border-top: 1px solid black;
    }
    .tasks {
      display: grid;
      grid-template-columns: auto auto auto;
      grid-template-rows: repeat(auto-file);
    }
    @media screen and (min-width:400px) {
      .day { grid-template-columns: auto auto; }
      .tasks { grid-template-columns: auto; }
    }
    @media screen and (min-width:1024px) {
      .day { grid-template-columns: 40% 1fr; }
      .tasks { grid-template-columns: auto auto; }
    }
    @media screen and (min-width:1250px) {
      .day { grid-template-columns: 30% 1fr; }
      .tasks { grid-template-columns: auto auto auto; }
    }
    @media screen and (min-width:1400px) {
      .day { grid-template-columns: 25% 1fr; }
      .tasks { grid-template-columns: auto auto auto auto; }
    }
    .day .project {
      grid-column: 1;
      grid-row: 1 / span 2;
    }
    .day .tasks {
      grid-column: 2;
      grid-row: 2;
    }
    .project h2 { margin-bottom: 2em; }
    .piechart {
      display: grid;
      grid-template-columns: auto 1fr;
      grid-template-rows: auto;
    }
    .piechart .pie { grid-column: 1; grid-row: 1; }
    .piechart table { grid-column: 2; grid-row: 1; }
    .hours { grid-column: 1 / span 2; grid-row: 2; }
    .hours h3 { margin-left: 5em; margin-bottom: 0.5ex; }
    table.legend { margin-left: 0.75em; margin-bottom: auto; }
    .legend span { margin-left: 0.25em; }
    .legend td {
        display: flex;
        align-items: center;
    }
"#;

// Utility function to return the current stack file.
fn stack(config: &Config) -> Result<Stack> {
    Ok(Stack::new(&config.stackfile())?)
}

// Utility function to return the current log file.
fn logfile(config: &Config) -> Result<Logfile> {
    Ok(Logfile::new(&config.logfile())?)
}

/// Initialize the timelog directory supplied and create a `.timelogrc` config file.
/// If no directory is supplied default to `~/timelog`
///
/// ## Errors
///
/// - Return [`PathError::CantCreatePath`] if cannot create timelog directory
/// - Return [`PathError::FileAccess`] if we are unable to write the configuration.
/// - Return [`PathError::InvalidPath`] if the path is not valid
pub fn initialize(config: &Config, dir: &Option<String>) -> std::result::Result<(), PathError> {
    let mut config = config.clone();
    let dir = dir.as_ref().unwrap_or_else(|| config.dir());
    fs::create_dir_all(dir)
        .map_err(|e| PathError::CantCreatePath(dir.to_string(), e.to_string()))?;

    let candir =
        fs::canonicalize(dir).map_err(|e| PathError::InvalidPath(dir.to_string(), e.to_string()))?;
    config.set_dir(candir.to_str().unwrap().to_owned());
    config.create()?;
    Ok(())
}

/// Start a timing event.
/// Add the event description to the logfile marked with the current date and time.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn start_event(config: &Config, args: &[String]) -> Result<()> {
    let logfile = logfile(config)?;
    Ok(logfile.add_task(&args.join(" "))?)
}

/// Stop timing an event.
/// Add the 'stop' entry to the logfile marked with the current date and time.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the log file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the log file.
pub fn stop_event(config: &Config) -> Result<()> {
    let logfile = logfile(config)?;
    Ok(logfile.add_task("stop")?)
}

/// Start a timing event and saving the current event to the stack.
/// Add the event description to the logfile marked with the current date and time.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn push_event(config: &Config, args: &[String]) -> Result<()> {
    let logfile = logfile(config)?;
    let event = logfile.last_event()?;
    if event.event_text().is_empty() {
        return Ok(());
    }

    let stack = stack(config)?;
    stack.push(&event.event_text())?;

    Ok(logfile.add_task(&args.join(" "))?)
}

/// Resume the previous task event by popping it off the stack and starting that task at the
/// current date and time.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn resume_event(config: &Config) -> Result<()> {
    let stack = stack(config)?;
    if let Some(task) = stack.pop() {
        logfile(config)?.add_task(&task)?;
    }
    Ok(())
}

/// Pause the current event by placing it on the stack and stopping timing.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn pause_event(config: &Config) -> Result<()> {
    push_event(config, &["stop".to_owned()])
}

/// Discard one or more items from the top of the event stack.
///
/// The `arg` is a [`DropArg`] that provides information about how many items to drop.
/// - [`DropArg::All`] clears the stack
/// - [`DorpArg::Num(n)`] drops `n` items from the stack
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn drop_from_stack(config: &Config, arg: DropArg) -> Result<()> {
    let stack = stack(config)?;
    match arg {
        DropArg::All => Ok(stack.clear()?),
        DropArg::Num(n) => stack.drop(n),
    }
}

/// List the events for a particular date, or today if none is supplied.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn list_events(config: &Config, date: &Option<String>) -> Result<()> {
    let file = logfile(config)?.open()?;

    let start = Date::parse(date.as_ref().unwrap_or(&String::from("today")))?;
    let end   = &start.succ().to_string();
    let start = start.to_string();

    for line in TaskLineIter::new(
        BufReader::new(file)
            .lines()
            .take_while(|ol| ol.is_ok())
            .map(|ol| ol.unwrap()),
        &start,
        end,
    )? {
        println!("{}", line);
    }
    Ok(())
}

/// List the projects in the logfile.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn list_projects(config: &Config) -> Result<()> {
    let mut projects: HashSet<String> = HashSet::new();

    let file = logfile(config)?.open()?;
    BufReader::new(file)
        .lines()
        .take_while(|ol| ol.is_ok())
        .map(|ol| ol.unwrap())
        .for_each(|ln| {
            if let Some(proj) = PROJECT_RE.captures(&ln) {
                projects.insert(proj.get(1).unwrap().as_str().to_owned());
            }
        });

    let mut names: Vec<String> = projects.iter().map(|p| p.to_string()).collect();
    names.as_mut_slice().sort();
    names.iter().for_each(|p| println!("{}", p));

    Ok(())
}

/// List the items on the stack, most recent first.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn list_stack(config: &Config) -> Result<()> {
    let stackfile = stack(config)?;
    if !stackfile.exists() {
        return Ok(());
    }

    let file = stackfile.open()?;
    let mut lines: Vec<String> = BufReader::new(file)
        .lines()
        .take_while(|ol| ol.is_ok())
        .map(|ol| ol.unwrap())
        .collect();
    lines.as_mut_slice().reverse();
    lines.iter().for_each(|ln| println!("{}", ln));

    Ok(())
}

/// Launch the configured editor to edit the logfile.
///
/// ## Errors
///
/// - Return [`Error::EditorFailed`] if unable to run the editor.
pub fn edit(config: &Config) -> Result<()> {
    Command::new(config.editor())
        .arg(config.logfile())
        .status()
        .map_err(|e| Error::EditorFailed(config.editor().to_string(), e.to_string()))?;

    Ok(())
}

// Generate a report from the events in the logfile based on the supplied `args`.
//
// Uses the supplied filter object to apply appropriate filtering conditions that
// select the events of interest from the logfile. These events are collected into
// [`Day`]s which are then reported using the supplied function `f`.
//
// The purpose of this method is to handle all of the boring grunt-work of finding the
// events in question and collecting them together to generate one or more daily reports.
//
// ## Errors
//
// - Return [`PathError::FilenameMissing`] if the log file is missing.
// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
// - Return [`PathError::FileAccess`] if the file cannot be opened.
// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
fn report<F>(config: &Config, filter: &dyn DayFilter, mut f: F) -> Result<()>
where
    F: FnMut(&Day) -> Result<()>,
{
    let file = logfile(config)?.open()?;

    // let filter = FilterArgs::new(args)?;
    let start = filter.start();
    let mut day = Day::new(&start)?;

    for line in TaskLineIter::new(
        BufReader::new(file)
            .lines()
            .take_while(|ol| ol.is_ok())
            .map(|ol| ol.unwrap()),
        &start,
        &filter.end(),
    )? {
        let event = Event::from_line(&line)?;
        let stamp = event.stamp();

        if day.date_stamp() != stamp {
            if let Some(filtered) = filter.filter_day(day) {
                f(&filtered)?;
            }

            day = Day::new(&stamp)?;
        }
        day.add_event(&event)?;
    }
    day.finish()?;

    if let Some(filtered) = filter.filter_day(day) {
        f(&filtered)?;
    }
    Ok(())
}

// Return a file object for the report file.
fn report_file(filename: &str) -> Result<fs::File> {
    Ok(OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(&filename)
            .map_err(|e| PathError::FileAccess(filename.to_string(), e.to_string()))?)
}

// Display the chart in the configured browser.
fn launch_chart(config: &Config, filename: &str) -> Result<()> {
    Command::new(config.browser())
        .arg(filename)
        .stderr(Stdio::null())    // discard output, for odd chromium message
        .status()
        .map_err(|e| Error::EditorFailed(config.browser().to_string(), e.to_string()))?;
    Ok(())
}

/// Create a graphical chart for each supplied day.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
pub fn chart_daily(config: &Config, args: &[String]) -> Result<()> {
    let filename = config.reportfile();
    let mut file = report_file(&filename)?;
    let mut w = EmitterConfig::new()
        .perform_indent(true)
        .write_document_declaration(false)
        .create_writer(&mut file);
    emit_xml!(&mut w, html => {
        emit_xml!(w, head => {
            emit_xml!(w, title; "Daily Timelog Report")?;
            emit_xml!(w, style, type: "text/css"; STYLESHEET)
        })?;
        emit_xml!(w, body => {
            report(config, &DateRangeArgs::new(args)?, |day|
                Ok(day.daily_chart().write(&mut w)?)
            )
        })
    })?;

    launch_chart(config, &filename)
}

/// Print the full daily report for each supplied day.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
pub fn report_daily(config: &Config, args: &[String]) -> Result<()> {
    report(config, &FilterArgs::new(args)?, |day| Ok(print!("{}", day.detail_report())))
}

/// Print the summary report for each supplied day.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
pub fn report_summary(config: &Config, args: &[String]) -> Result<()> {
    report(config, &FilterArgs::new(args)?, |day| Ok(print!("{}", day.summary_report())) )
}

/// Print the hourly report for each supplied day.
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::WrongDateOrder`] if the start date is not before the end date
pub fn report_hours(config: &Config, args: &[String]) -> Result<()> {
    report(config, &FilterArgs::new(args)?, |day| Ok(print!("{}", day.hours_report())) )
}

/// Display the current task
///
/// ## Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn current_task(config: &Config) -> Result<()> {
    let event = logfile(config)?.last_event()?;
    if event.is_stop() {
        println!("Not in event.");
    }
    else {
        println!("{}", &event);
        let dur = DateTime::now().sub(&event.date_time())?;
        println!("Duration: {}", format_dur(&dur));
    }

    Ok(())
}

/// Archive the first year from the logfile (if not the current year).
///
/// ## Errors
///
/// - Return [`Error::PathError`] for any error accessing the log or archive files.
pub fn archive_year(config: &Config) -> Result<()> {
    match Archiver::new(config).archive()? {
        None => println!("Nothing to archive"),
        Some(year) => println!("{} archived", year),
    }
    Ok(())
}

/// List the aliases from the config file.
pub fn list_aliases(config: &Config) -> Result<()> {
    let mut aliases: Vec<&String> = config.alias_names().collect();
    aliases.as_mut_slice().sort();
    let maxlen: usize = aliases.iter().map(|a| a.len()).max().unwrap_or_default();
    println!("Aliases:");
    for alias in aliases {
        println!(
            "  {1:0$} : {2}",
            &maxlen,
            &alias,
            config.alias(alias).unwrap()
        );
    }

    Ok(())
}
