//! Support for accessing the timelog logic from a tool
//!
//! # Examples
//!
//! ```rust,no_run
//! use structopt::StructOpt;
//!
//! use timelog::Cli;
//!
//! # fn main() {
//! let cli = Cli::from_args();
//! let _ = cli.run();
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Cli`] struct handling the command line processing for the main program.
use std::iter::once;
use std::path::PathBuf;

use crate::config::{Config, DEFAULT_CONF, DEFAULT_DIR, DEFAULT_EDITOR, DEFAULT_BROWSER};
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;
use crate::Result;

use lazy_static::lazy_static;
use regex::Regex;
use structopt::StructOpt;

pub mod args;
pub mod cmd;

pub type DropArg = args::DropArg;
pub type FilterArgs = args::FilterArgs;
pub type DateRangeArgs = args::DateRangeArgs;

/// Name of the program binary.
const BIN_NAME: &str = "rtimelog";

lazy_static! {
    /// Regular expression for matching the alias template match
    static ref SUBST_RE: Regex = Regex::new(r#"\{\}"#).unwrap();
}

// Utility type for a vector of strings
#[derive(Debug, PartialEq, StructOpt)]
struct VecString {
    args: Vec<String>,
}

// Utility type for an optional string
#[derive(Debug, PartialEq, StructOpt)]
struct OptString {
    arg: Option<String>,
}

// Enumeration for determining whether to expand aliases.
enum ExpandAlias {
    Yes,
    No,
}

// Help message explaining an event description
const EVENT_DESC: &str = r" The command takes a 'event description' consisting of an optional project formatted with a
 leading '+', an optional task name formatted with a leading '@', and potentially more text adding
 details to the task. If no task name starting with '@' is supplied, any extra text is treated as
 the task.";

// Help message explaining the format of an event description.
const DATE_DESC: &str = r"The 'date description' consists of a date of the form 'YYYY-MM-DD',
 or one of a set of relative date strings including: today, yesterday, sunday, monday, tuesday, wednesday,
 thursday, friday, or saturday. The first two are obvious. The others refer to the previous instance of that
 day.";

/// Specify the subcommands supported by the program.
#[derive(Debug, PartialEq, StructOpt)]
enum Subcommands {
    /// Create the timelog directory and configuration.
    ///
    /// Use the supplied directory, or default to `~/timelog` if not supplied.
    #[structopt(usage="init [dir]")]
    Init(OptString),

    /// Start timing a new event.
    ///
    /// Stop timing the current event (if any) and start timing a new event.
    #[structopt(usage="start {event description}", after_help=EVENT_DESC)]
    Start(VecString),

    /// Stop timing the current event.
    Stop,

    /// Save the current event and start timing a new event.
    ///
    /// This command works the same as the `start` command, except that the
    /// current command is saved on the top of the stack. This makes resuming
    /// the previous command easier.
    #[structopt(usage="push {event description}", after_help=EVENT_DESC)]
    Push(VecString),

    /// Stop last event and restart top event on stack.
    Pop,

    /// Stop last event and restart top event on stack.
    Resume,

    /// Save the current event and stop timing.
    Pause,

    /// Drop items from stack.
    #[structopt(
        usage = "drop [all|{num}]",
        after_help = r"
    - If the 'all' argument is passed, this command clears the stack of all events.
    - If an positive integer is supplied, that number of items is dropped from the top of the stack.
    - If no argument is supplied, drop only the top item."
    )]
    Drop(OptString),

    /// List events for the specified day. Default to today.
    #[structopt(usage="ls {date_desc}", after_help=DATE_DESC)]
    Ls(OptString),

    /// List known projects.
    Lsproj,

    /// Display items on the stack.
    Lstk,

    /// Open the timelog file in the current editor.
    Edit,

    /// Display a report for the specified days and projects.
    #[structopt(usage="report [date_desc [end_date_desc]] [project_regex]...", after_help=DATE_DESC)]
    Report(VecString),

    /// Display a summary of the appropriate days' projects.
    #[structopt(usage="summary [date_desc [end_date_desc]] [project_regex]...", after_help=DATE_DESC)]
    Summary(VecString),

    /// Display the hours worked for each of the appropriate days and projects.
    #[structopt(usage="hours [date_desc [end_date_desc]] [project_regex]...", after_help=DATE_DESC)]
    Hours(VecString),

    /// Display a chart of the hours worked for each of the appropriate days and projects.
    #[structopt(usage="chart [date_desc [end_date_desc]]", after_help=DATE_DESC)]
    Chart(VecString),

    /// Display the current event.
    Curr,

    /// Archive the first year from the timelog file, as long as it isn't the current year.
    Archive,

    /// List the aliases from the config file.
    Aliases,

    // `external_subcommand` tells structopt to put
    // all the extra arguments into this Vec
    #[structopt(external_subcommand)]
    Other(Vec<String>),
}

/// Specify all of the command line parameters supported by the program.
#[derive(StructOpt, Debug)]
#[structopt(
    about,
    after_help = r"
 Some commands take a 'event description' consisting of an optional project formatted with a
 leading '+', an optional task name formatted with a leading '@', and potentially more text adding
 details to the task. If no task name starting with '@' is supplied, any extra text is treated as
 the task.

 Some commands take one or two 'date descriptions'. This description consists of a date of the form 'YYYY-MM-DD',
 or one of a set of relative date strings including: today, yesterday, sunday, monday, tuesday, wednesday,
 thursday, friday, or saturday. The first two are obvious. The others refer to the previous instance of that
 day."
)]
pub struct Cli {
    /// Specify a directory for logging the task events and stack.
    #[structopt(long, parse(from_os_str), default_value = &DEFAULT_DIR)]
    dir: PathBuf,

    /// Specify the editor to use for modifying events.
    #[structopt(long, parse(from_os_str), default_value = &DEFAULT_EDITOR)]
    editor: PathBuf,

    /// Specify the path to the configuration file.
    #[structopt(long, parse(from_os_str), default_value = &DEFAULT_CONF)]
    conf: PathBuf,

    /// Specify the command to execute the browser.
    #[structopt(long, default_value = &DEFAULT_BROWSER)]
    browser: String,

    /// Sub-commands which determine what actions to take
    #[structopt(subcommand)]
    cmd: Subcommands,
}

impl Cli {
    /// Execute the action specified on the command line.
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FilenameMissing`] if no configuration file is known.
    /// - Return [`PathError::InvalidPath`] if the timelog directory is not a valid path.
    /// - Return [`PathError::FilenameMissing`] if no editor has been configured.
    /// - Return other errors specific to the commands.
    pub fn run(&self) -> Result<()> {
        let config = self.config()?;
        self.cmd.run(&config, ExpandAlias::Yes)
    }

    // Retrieve the configuration from the file.
    //
    // ## Errors
    //
    // - Return [`PathError::FilenameMissing`] if no configuration file is known.
    // - Return [`PathError::InvalidPath`] if the timelog directory is not a valid path.
    // - Return [`PathError::FilenameMissing`] if no editor has been configured.
    fn config(&self) -> std::result::Result<Config, PathError> {
        let conf_file = self.conf.to_str().ok_or(PathError::FilenameMissing)?;
        match Config::from_file(conf_file) {
            Ok(config) => Ok(config),
            Err(_) => {
                let mut config = Config::default();
                config.set_dir(
                    self.dir
                        .to_str()
                        .ok_or_else(|| PathError::InvalidPath(String::new(), String::new()))?
                        .to_string(),
                );
                config.set_editor(
                    self.editor
                        .to_str()
                        .ok_or(PathError::FilenameMissing)?
                        .to_string(),
                );
                config.set_browser(self.browser.clone());
                Ok(config)
            }
        }
    }
}

impl Subcommands {
    /// Execute the action associated with the current variant.
    ///
    /// ## Errors
    ///
    /// - Return [`Error`]s for particular commands failing.
    /// - Return [`Error::InvalidCommand`] if the subcommand is not recognized and no alias
    /// matches.
    pub fn run(&self, config: &Config, expand: ExpandAlias) -> Result<()> {
        use Subcommands::*;

        match &self {
            Init(arg) => Ok(cmd::initialize(config, &arg.arg)?),
            Start(args) => cmd::start_event(config, &args.args),
            Stop => cmd::stop_event(config),
            Push(args) => cmd::push_event(config, &args.args),
            Pop => cmd::resume_event(config),
            Resume => cmd::resume_event(config),
            Pause => cmd::pause_event(config),
            Drop(arg) => cmd::drop_from_stack(config, DropArg::parse(&arg.arg)?),
            Ls(arg) => cmd::list_events(config, &arg.arg),
            Lsproj => cmd::list_projects(config),
            Lstk => cmd::list_stack(config),
            Edit => cmd::edit(config),
            Report(args) => cmd::report_daily(config, &args.args),
            Summary(args) => cmd::report_summary(config, &args.args),
            Hours(args) => cmd::report_hours(config, &args.args),
            Chart(args) => cmd::chart_daily(config, &args.args),
            Curr => cmd::current_task(config),
            Archive => cmd::archive_year(config),
            Aliases => cmd::list_aliases(config),
            Other(args) => match expand {
                ExpandAlias::Yes => self.expand_alias(config, args),
                ExpandAlias::No => Err(Error::InvalidCommand(args[0].to_owned())),
            },
        }
    }

    // Replace an alias as the first argument with the definition of the alias.
    fn expand_alias(&self, config: &Config, args: &[String]) -> Result<()> {
        let alias = &args[0];
        let mut args_iter = args[1..].iter().map(|s| s.as_str());

        let expand: Vec<String> = config.alias(alias)
            .ok_or_else(|| Error::InvalidCommand(alias.to_owned()))?
            .split(' ')
            .map(|w| {
                if SUBST_RE.is_match(w) {
                    args_iter.next()
                        .map(|val| SUBST_RE.replace(w, val).into_owned())
                        .unwrap_or_else(|| w.to_string())
                }
                else {
                    w.to_string()
                }
            })
            .collect();

        let cmd =
            Subcommands::from_iter(once(BIN_NAME)
                .chain(expand.iter().map(|s| s.as_str()))
                .chain(args_iter));
        cmd.run(config, ExpandAlias::No)
    }
}
