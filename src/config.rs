//! Configuration file definition
//!
//! # Examples
//!
//! ```rust
//! use timelog::{Config, Event, Result};
//!
//! # fn main() -> Result<()> {
//! # let events: Vec<Event> = vec![];
//! # let mut event_iter = events.into_iter();
//! let conf = Config::from_file("~/.timelogrc")?;
//!
//! println!("Dir: {}", conf.dir());
//! println!("Logfile: {}", conf.logfile());
//! println!("Editor: {}", conf.editor());
//! #   Ok(())
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Config`] struct represents the configuration file information.

use std::cmp::PartialEq;
use std::collections::HashMap;
use std::env;
use std::fmt::Debug;
use std::fs::{canonicalize, OpenOptions};
use std::io::prelude::*;
use std::io::BufWriter;
use std::path::PathBuf;

use configparser::ini::Ini;
use getset::{Getters, Setters};
use lazy_static::lazy_static;
use tilde_expand::tilde_expand;

#[doc(inline)]
use crate::error::PathError;
use crate::Result;

lazy_static! {
    /// Default path to the timelog config file
    pub static ref DEFAULT_CONF: String = normalize_path("~/.timelogrc").unwrap();
    /// Default path to the timelog log directory
    pub static ref DEFAULT_DIR: String = normalize_path("~/timelog").unwrap();
    /// Default command to open the editor
    pub static ref DEFAULT_EDITOR: String = env::var("VISUAL")
        .or_else(|_| env::var("EDITOR"))
        .unwrap_or_else(|_| String::from("vim"));
}

/// Default command to open the browser
#[cfg(target_os = "macos")]
pub const DEFAULT_BROWSER: &str = "open";

/// Default command to open the browser
#[cfg(not(target_os = "macos"))]
pub const DEFAULT_BROWSER: &str = "chromium-browser";

/// Type specifying the configuration for the timelog program.
#[derive(Getters, Setters, Clone, Debug, PartialEq)]
pub struct Config {
    /// The name of the configuration file
    #[getset(get = "pub")]
    configfile: String,
    /// The path of the directory that stores the log and stack files
    #[getset(get = "pub", set = "pub")]
    dir: String,
    /// The path to the editor used with the `edit` command
    #[getset(get = "pub", set = "pub")]
    editor: String,
    /// The path to the browser used with the `chart` command
    #[getset(get = "pub", set = "pub")]
    browser: String,
    /// The default command if none is entered
    #[getset(get = "pub")]
    defcmd: String,
    /// List of aliases to extend the commands
    aliases: HashMap<String, String>,
}

/// Utility function to handle tilde expansion of a path.
///
/// ## Errors
///
/// - Return a [`PathError::InvalidPath`] if there are invalid characters in the path
pub fn normalize_path(filename: &str) -> std::result::Result<String, PathError> {
    // If this fails, the user directory must contain invalid characters.
    String::from_utf8(tilde_expand(filename.as_bytes()))
        .map_err(|e| PathError::InvalidPath(filename.to_owned(), e.to_string()))
}

// Ensure the filename either exists or could be created in the supplied directory
//
// ## Errors
//
// - Return [`PathError::FilenameMissing`] if the filename does not exist.
// - Return [`PathError::InvalidPath`] if the dirname portion of the file is not valid.
fn ensure_filename(file: &str) -> std::result::Result<String, PathError> {
    if file.is_empty() {
        return Err(PathError::FilenameMissing);
    }
    let mut dir = PathBuf::from(normalize_path(file)?);
    let filename = dir
        .file_name()
        .ok_or(PathError::FilenameMissing)?
        .to_os_string();
    dir.pop();

    let mut candir =
        canonicalize(dir).map_err(|e| PathError::InvalidPath(file.to_owned(), e.to_string()))?;
    candir.push(filename);
    Ok(candir.to_str().unwrap().to_owned())
}

impl Default for Config {
    /// Create a [`Config`] with all of the default parameters.
    ///
    /// ## Panics
    ///
    /// If any of the default valures are not legal in the current system, the code will
    /// panic. These is a programmer-specified defaults, and should never fail.
    fn default() -> Self {
        Self {
            configfile: normalize_path("~/.timelogrc").expect("Invalid config file"),
            dir: normalize_path("~/timelog").expect("Invalid user dir"),
            editor: "vim".into(),
            browser: DEFAULT_BROWSER.into(),
            defcmd: "list".into(),
            aliases: HashMap::new(),
        }
    }
}

// Utility function for extracting conf data.
fn conf_get<'a>(base: &'a HashMap<String, Option<String>>, key: &'static str) -> Option<&'a str> {
    match base.get(key) {
        Some(Some(val)) => Some(val),
        _ => None,
    }
}

impl Config {
    /// Create a new [`Config`] object with supplied parameters
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::InvalidConfigPath`] if the configuration filename is not valid.
    /// - Return [`PathError::InvalidTimelogPath`] if the timelog directory is not valid.
    pub fn new(config: &str, dir: Option<&str>, editor: Option<&str>, browser: Option<&str>, cmd: Option<&str>) -> Result<Self> {
        let dir = dir.unwrap_or("~/timelog");
        Ok(Self {
            configfile: normalize_path(config).map_err(|_| PathError::InvalidConfigPath)?,
            dir: normalize_path(dir).map_err(|_| PathError::InvalidTimelogPath)?,
            editor: editor.unwrap_or("vim").into(),
            browser: browser.unwrap_or(DEFAULT_BROWSER).into(),
            defcmd: cmd.unwrap_or("list").into(),
            aliases: HashMap::new(),
        })
    }

    /// Create a new [`Config`] object from the supplied config file
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::InvalidConfigPath`] if the configuration filename is not valid.
    /// - Return [`PathError::InvalidTimelogPath`] if the timelog directory is not valid.
    /// - Return [`PathError::FileAccess`] if the config file is not accessible.
    pub fn from_file(filename: &str) -> Result<Self> {
        let configfile = ensure_filename(filename)?;
        let mut parser = Ini::new();
        let config = parser
            .load(&configfile)
            .map_err(|e| PathError::FileAccess(configfile.to_owned(), e))?;

        let default = HashMap::new();
        let base = match config.get("default") {
            Some(hash) => hash,
            _ => &default,
        };
        let mut conf = Config::new(
            &configfile,
            conf_get(base, "dir"),
            conf_get(base, "editor"),
            conf_get(base, "browser"),
            conf_get(base, "defcmd"),
        )?;
        if let Some(aliases) = config.get("alias") {
            aliases.iter().for_each(|(k, v)| {
                conf.set_alias(k, v.as_ref().unwrap());
            });
        }

        Ok(conf)
    }

    /// The file containing the timelog entries
    pub fn logfile(&self) -> String {
        format!("{}/timelog.txt", self.dir)
    }

    /// The file that holds the stack
    pub fn stackfile(&self) -> String {
        format!("{}/stack.txt", self.dir)
    }

    /// The file containing the timelog entries
    pub fn reportfile(&self) -> String {
        format!("{}/report.html", self.dir)
    }

    /// Write the configuration to disk in the file specified by the [`Config`]
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if we are unable to write the configuration.
    pub fn create(&self) -> std::result::Result<(), PathError> {
        let configfile = self.configfile();
        let file = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(configfile)
            .map_err(|e| PathError::FileAccess(configfile.to_string(), e.to_string()))?;
        let mut stream = BufWriter::new(file);

        writeln!(&mut stream, "dir={}", self.dir())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        writeln!(&mut stream, "editor={}", self.editor())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        writeln!(&mut stream, "browser={}", self.browser())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        writeln!(&mut stream, "defcmd={}", self.defcmd())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;

        writeln!(&mut stream, "\n[alias]")
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        for (key, val) in self.aliases.iter() {
            writeln!(stream, "    {} = {}", key, val.to_owned())
                .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        }

        stream
            .flush()
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        Ok(())
    }

    /// Return an iterator over the alias names
    pub fn alias_names(&self) -> impl Iterator<Item=&'_ String> {
        self.aliases.keys()
    }

    /// Retrieve the value associate with the named alias, if one exists.
    pub fn alias(&self, key: &str) -> Option<&String> {
        self.aliases.get(key)
    }

    /// Set the value of the named alias
    fn set_alias(&mut self, name: &str, val: &str) {
        self.aliases.insert(name.to_owned(), val.to_owned());
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;
    use std::fs::File;
    use tempfile::TempDir;

    #[test]
    fn test_default() {
        let config = Config::default();
        assert_that!(config.dir()).is_equal_to(&normalize_path("~/timelog").unwrap());
        assert_that!(config.logfile())
            .is_equal_to(&normalize_path("~/timelog/timelog.txt").unwrap());
        assert_that!(config.stackfile())
            .is_equal_to(&normalize_path("~/timelog/stack.txt").unwrap());
        assert_that!(config.reportfile())
            .is_equal_to(&normalize_path("~/timelog/report.html").unwrap());
        assert_that!(config.editor()).is_equal_to(&"vim".to_owned());
        assert_that!(config.browser()).is_equal_to(&DEFAULT_BROWSER.to_owned());
        assert_that!(config.defcmd()).is_equal_to(&"list".to_owned());
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_new() {
        let config =
            Config::new("~/.timelogrc", Some("~/timelog"), Some("vim"), Some("chromium-browser"), Some("list")).unwrap();
        assert_that!(config.dir()).is_equal_to(&normalize_path("~/timelog").unwrap());
        assert_that!(config.logfile())
            .is_equal_to(&normalize_path("~/timelog/timelog.txt").unwrap());
        assert_that!(config.stackfile())
            .is_equal_to(&normalize_path("~/timelog/stack.txt").unwrap());
        assert_that!(config.reportfile())
            .is_equal_to(&normalize_path("~/timelog/report.html").unwrap());
        assert_that!(config.editor()).is_equal_to(&"vim".to_owned());
        assert_that!(config.browser()).is_equal_to(&"chromium-browser".to_owned());
        assert_that!(config.defcmd()).is_equal_to(&"list".to_owned());
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_from_file_dir_only() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let path = tmpdir.path();
        let path_str = path.to_str().unwrap();

        let filename = format!("{}/.timerc", path_str);
        let mut file = File::create(&filename).unwrap();
        let _ = file.write_all(format!("dir = {}", path_str).as_bytes());

        let config = Config::from_file(&filename).unwrap();
        let expect_log = normalize_path(format!("{}/timelog.txt", path_str).as_str()).unwrap();
        let expect_stack = normalize_path(format!("{}/stack.txt", path_str).as_str()).unwrap();
        let expect_report = normalize_path(format!("{}/report.html", path_str).as_str()).unwrap();
        assert_that!(config.dir()).is_equal_to(&path_str.to_owned());
        assert_that!(config.logfile()).is_equal_to(&expect_log);
        assert_that!(config.stackfile()).is_equal_to(&expect_stack);
        assert_that!(config.reportfile()).is_equal_to(&expect_report);
        assert_that!(config.editor()).is_equal_to(&"vim".to_owned());
        assert_that!(config.browser()).is_equal_to(&DEFAULT_BROWSER.to_owned());
        assert_that!(config.defcmd()).is_equal_to(&"list".to_owned());
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_from_file_base() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let path = tmpdir.path();
        let path_str = path.to_str().unwrap();

        let filename = format!("{}/.timerc", path_str);
        let mut file = File::create(&filename).unwrap();
        let output = format!("dir={}\neditor=nano\nbrowser=firefox\ndefcmd=stop", path_str);
        let _ = file.write_all(output.as_bytes());

        let config = Config::from_file(&filename).unwrap();
        let expect_log = normalize_path(format!("{}/timelog.txt", path_str).as_str()).unwrap();
        let expect_stack = normalize_path(format!("{}/stack.txt", path_str).as_str()).unwrap();
        let expect_report = normalize_path(format!("{}/report.html", path_str).as_str()).unwrap();
        assert_that!(config.dir()).is_equal_to(&normalize_path(path_str).unwrap().to_owned());
        assert_that!(config.logfile()).is_equal_to(&expect_log);
        assert_that!(config.stackfile()).is_equal_to(&expect_stack);
        assert_that!(config.reportfile()).is_equal_to(&expect_report);
        assert_that!(config.editor()).is_equal_to(&"nano".to_owned());
        assert_that!(config.browser()).is_equal_to(&"firefox".to_owned());
        assert_that!(config.defcmd()).is_equal_to(&"stop".to_owned());
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_from_file_aliases() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let path = tmpdir.path();
        let path_str = path.to_str().unwrap();

        let filename = format!("{}/.timerc", path_str);
        let mut file = File::create(&filename).unwrap();
        let _ = file.write_all(b"[alias]\na=start +play @A\nb=start +work @B");

        let config = Config::from_file(&filename).unwrap();
        let mut names: Vec<&String> = config.alias_names().collect();
        names.sort();
        assert_that!(names).is_equal_to(vec![&"a".to_owned(), &"b".to_owned()]);
        assert_that!(config.alias("a"))
            .is_some()
            .is_equal_to(&"start +play @A".to_owned());
        assert_that!(config.alias("b"))
            .is_some()
            .is_equal_to(&"start +work @B".to_owned());
    }
}
