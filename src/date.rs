//! Utilities for working with dates and times
//!
//! # Examples
//!
//! ```rust
//! use timelog::date::{Date, DateTime};
//! use timelog::Result;
//!
//! # fn main() -> Result<()> {
//! let mut day = Date::try_from("2021-07-02")?;
//! print!("Date: {}", day);
//!
//! let mut stamp = DateTime::try_from("2021-07-02 12:34:00")?;
//! print!("Stamp: {}", stamp);
//!
//! let mut today = Date::parse("today")?;
//! print!("Today: {}", today);
//! #   Ok(())
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Date`] struct represents a date in the local time zone. The module also
//! provides tools for manipulating [`Date`]s.
//!
//! The [`DateTime`] struct represents a date and time in the local time zone. The
//! module also provides tools for manipulating [`DateTime`]s.

use std::cmp::{Eq, Ord, PartialEq, PartialOrd};
use std::convert::From;
use std::fmt::{self, Display};
use std::time::Duration;

use chrono::{Datelike, NaiveDate, NaiveDateTime, Timelike, Weekday};
use chrono::{Local, LocalResult, TimeZone};
use thiserror::Error;

/// Representation of a date in the local time zone.
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct Date(chrono::Date<Local>);

/// Representation of a date and time in the local time zone.
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct DateTime(chrono::DateTime<Local>);

/// Errors related to dates and times.
#[derive(Error, Debug, PartialEq)]
pub enum Error {
    /// Invalid day specification
    #[error("'{0}' is not a valid day specification")]
    InvalidDaySpec(String),

    /// Invalid stamp format on an event line.
    #[error("Invalid stamp format.")]
    InvalidDate,

    /// Event line is encountered that is before a previous event line.
    #[error("Events out of order.")]
    EventOrder,
}

/// Result type for errors in date functionality.
pub type Result<T> = std::result::Result<T, Error>;

// Convert weekday string into appropriate Weekday variant.
fn weekday_from_str(day: &str) -> Result<Weekday> {
    match day {
        "sunday" => Ok(Weekday::Sun),
        "monday" => Ok(Weekday::Mon),
        "tuesday" => Ok(Weekday::Tue),
        "wednesday" => Ok(Weekday::Wed),
        "thursday" => Ok(Weekday::Thu),
        "friday" => Ok(Weekday::Fri),
        "saturday" => Ok(Weekday::Sat),
        _ => Err(Error::InvalidDaySpec(day.to_owned())),
    }
}

// Convert Weekday variabt into appropriate weekday string.
fn weekday_str(day: Weekday) -> &'static str {
    match day {
        Weekday::Sun => "Sunday",
        Weekday::Mon => "Monday",
        Weekday::Tue => "Tuesday",
        Weekday::Wed => "Wednesday",
        Weekday::Thu => "Thursday",
        Weekday::Fri => "Friday",
        Weekday::Sat => "Saturday",
    }
}

impl Date {
    /// Create a [`Date`] out of a year, month, and day, returning a [`Result`].
    ///
    /// ## Errors
    ///
    /// If one of the parameters is outside the legal range for a date, returns an
    /// [`Error::InvalidDate`].
    pub fn new(year: u32, month: u32, day: u32) -> Result<Self> {
        match Local.ymd_opt(year as i32, month, day) {
            LocalResult::Single(d) => Ok(Self(d)),
            _ => Err(Error::InvalidDate),
        }
    }

    /// Create a [`Date`] for today.
    pub fn today() -> Self {
        Self(Local::today())
    }

    // Find the last date before this one where the day of the week was
    // weekday.
    fn find_previous(&self, weekday: Weekday) -> Self {
        let mut day = self.0.pred();
        while day.weekday() != weekday {
            day = day.pred();
        }
        Self(day)
    }

    /// Create a [`Date`] object from the supplied date specification.
    ///
    /// Legal specifications include "today" and "yesterday", the days of the week "sunday"
    /// through "saturday", and a date in the form "YYYY-MM-DD".
    /// The days of the week result in the date representing the previous instance of that day
    /// (last Monday for "monday", etc.).
    ///
    /// ## Errors
    ///
    /// Return [`Error::InvalidDaySpec`] if the supplied spec is not valid.
    pub fn parse(datespec: &str) -> Result<Self> {
        match datespec {
            "today" => Ok(Self::today()),
            "yesterday" => Ok(Self::today().pred()),
            "sunday" | "monday" | "tuesday" | "wednesday" | "thursday" | "friday" | "saturday" =>
                Ok(Self::today().find_previous(weekday_from_str(datespec)?)),
            _ => Self::try_from(datespec),
        }
    }

    /// Create a [`DateTime`] object for the last second of the current [`Date`]
    pub fn day_end(&self) -> DateTime {
        DateTime(self.0.and_hms(23, 59, 59))
    }

    /// Create a [`Date`] for the day after the current date.
    pub fn succ(&self) -> Date {
        Self(self.0.succ())
    }

    /// Create a [`Date`] for the day before the current date.
    pub fn pred(&self) -> Date {
        Self(self.0.pred())
    }

    /// Return the year portion of the [`Date`]
    pub fn year(&self) -> u32 {
        self.0.year() as u32
    }

    /// Return the month portion of the [`Date`]
    pub fn month(&self) -> u32 {
        self.0.month()
    }

    /// Return the day portion of the [`Date`]
    pub fn day(&self) -> u32 {
        self.0.day()
    }

    /// Return the day of the week as a string.
    pub fn weekday(&self) -> &'static str {
        weekday_str(self.0.weekday())
    }
}

impl std::convert::TryFrom<&str> for Date {
    type Error = Error;

    /// Create a [`Date`] out of a string, returning a [`Result`]
    ///
    /// ## Errors
    ///
    /// If the date is not formatted as 'YYYY-MM-DD', returns an [`Error::InvalidDate`].
    /// Also if the date string cannot be converted into a [`Date`], returns an [`Error::InvalidDate`].
    fn try_from(date: &str) -> Result<Self> {
        let parsed: NaiveDate = NaiveDate::parse_from_str(date, "%Y-%m-%d").map_err(|_| Error::InvalidDate)?;
        Ok(Self(Local.from_local_date(&parsed).single().ok_or(Error::InvalidDate)?))
    }
}

impl Display for Date {
    /// Format a [`Date`] object in "YYYY-MM-DD" format.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{:02}-{:02}", self.0.year(), self.0.month(), self.0.day())
    }
}

impl From<Date> for String {
    /// Convert a [`Date`] into a [`String`]
    fn from(date: Date) -> Self {
        date.to_string()
    }
}

impl DateTime {
    /// Create a [`DateTime`] from two tuples, one representing the date (year, month, day) and
    /// the second representing the time (hour, minute, second).
    ///
    /// ## Errors
    ///
    /// Return an [`Error::InvalidDate`] if the values in the tuples are not appropriate for the
    /// data types.
    pub fn new(date: (u32, u32, u32), time: (u32, u32, u32)) -> Result<Self> {
        let dt = match Local.ymd_opt(date.0 as i32, date.1, date.2) {
            LocalResult::Single(d) => d
                .and_hms_opt(time.0, time.1, time.2)
                .ok_or(Error::InvalidDate)?,
            _ => return Err(Error::InvalidDate),
        };
        Ok(Self(dt))
    }

    /// Return the epoch time representing the current value of the [`DateTime`] object.
    pub fn timestamp(&self) -> i64 {
        self.0.timestamp()
    }

    /// Return a copy of just the [`Date`] portion of the [`DateTime`] object.
    pub fn date(&self) -> Date {
        Date(self.0.date())
    }

    /// Create a [`Date`] for right now.
    pub fn now() -> Self {
        Self(Local::now())
    }

    /// Return the hour of the day.
    pub fn hour(&self) -> u32 {
        self.0.hour()
    }

    /// Return the number of seconds after the hour.
    pub fn second_offset(&self) -> u32 {
        self.0.minute() * 60 + self.0.second()
    }

    /// Return the [`Duration`] as a [`Result`] resulting from subtracting the `rhs` from the
    /// object.
    ///
    /// ## Errors
    ///
    /// Return an [`Error::EventOrder`] if the `rhs` is larger than the [`DateTime`].
    pub fn sub(&self, rhs: &Self) -> Result<Duration> {
        (self.0 - rhs.0).to_std().map_err(|_| Error::EventOrder)
    }

    /// Return a [`DateTime`] as a [`Result`] resulting from adding the `rhs` [`Duration`] to the
    /// object.
    ///
    /// ## Errors
    ///
    /// Return an [`Error::InvalidDate`] if adding the duration results in a bad date.
    pub fn add(&self, rhs: Duration) -> Result<Self> {
        Ok(Self(
            self.0 + chrono::Duration::from_std(rhs).map_err(|_| Error::InvalidDate)?,
        ))
    }

    /// Return a [`Duration`] lasting the supplied number of minutes.
    pub fn seconds(seconds: u64) -> Duration {
        Duration::from_secs(seconds)
    }

    /// Return a [`Duration`] lasting the supplied number of minutes.
    pub fn minutes(minutes: u64) -> Duration {
        Duration::from_secs(minutes * 60)
    }

    /// Return a [`Duration`] lasting the supplied number of hours.
    pub fn hours(hours: u64) -> Duration {
        Duration::from_secs(hours * 3600)
    }

    /// Return a [`Duration`] lasting the supplied number of days.
    pub fn days(days: u64) -> Duration {
        Duration::from_secs(days * 86400)
    }

    /// Return a [`Duration`] lasting the supplied number of weeks.
    pub fn weeks(weeks: u64) -> Duration {
        Self::days(weeks * 7)
    }
}

impl std::convert::TryFrom<&str> for DateTime {
    type Error = Error;

    /// Parse the [`DateTime`] out of a string, returning a [`Result`]
    ///
    /// ## Errors
    ///
    /// If the datetime is not formatted as 'YYYY-MM-DD HH:MM:SS', returns an [`Error::InvalidDate`].
    /// Also if the datetime cannot be converted into a [`DateTime`], returns an [`Error::InvalidDate`].
    fn try_from(datetime: &str) -> Result<Self> {
        let parsed: NaiveDateTime = NaiveDateTime::parse_from_str(datetime, "%Y-%m-%d %H:%M:%S").map_err(|_| Error::InvalidDate)?;
        Ok(Self(Local.from_local_datetime(&parsed).single().ok_or(Error::InvalidDate)?))
    }
}

impl Display for DateTime {
    /// Format a [`DateTime`] object in "YYYY-MM-DD HH:MM:DD" format.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{:02}-{:02} {:02}:{:02}:{:02}",
            self.0.year(), self.0.month(), self.0.day(),
            self.0.hour(), self.0.minute(), self.0.second())
    }
}

impl From<DateTime> for String {
    /// Convert a [`Date`] into a [`String`]
    fn from(datetime: DateTime) -> Self {
        datetime.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_date_new() {
        let date = Date::new(2021, 11, 20);
        assert_that!(&date).is_ok();
        assert_that!(date.unwrap().to_string()).is_equal_to(&String::from("2021-11-20"));
    }

    #[test]
    fn test_date_new_bad_month() {
        assert_that!(Date::new(2021, 0, 20)).is_err_containing(&Error::InvalidDate);
        assert_that!(Date::new(2021, 13, 20)).is_err_containing(&Error::InvalidDate);
    }

    #[test]
    fn test_date_new_bad_day() {
        assert_that!(Date::new(2021, 11, 0)).is_err_containing(&Error::InvalidDate);
        assert_that!(Date::new(2021, 11, 32)).is_err_containing(&Error::InvalidDate);
    }

    #[test]
    fn test_date_parse_today() {
        assert_that!(Date::parse("today"))
            .is_ok()
            .is_equal_to(&Date::today());
    }

    #[test]
    fn test_date_parse_yesterday() {
        assert_that!(Date::parse("yesterday"))
            .is_ok()
            .is_equal_to(&Date::today().pred());
    }

    #[test]
    fn test_date_parse_weekdays() {
        let max_dur = DateTime::days(7);
        let days: [&str; 7] = [ "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ];
        let today = Date::today();
        let midnight = Date::today().day_end();
        days.iter().for_each(|&day| {
            let date = Date::parse(day);
            assert_that!(date).is_ok().is_less_than(&today);

            let end_of_date = date.unwrap().day_end();
            assert_that!(midnight.sub(&end_of_date))
                .is_ok()
                .is_less_than_or_equal_to(&max_dur);
        });
    }

    #[test]
    fn test_date_day_end() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.day_end())
            .is_equal_to(&DateTime::new((2021, 11, 20), (23, 59, 59)).unwrap());
    }

    #[test]
    fn test_date_succ() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.succ()).is_equal_to(&Date::new(2021, 11, 21).unwrap());
    }

    #[test]
    fn test_date_pred() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.pred()).is_equal_to(&Date::new(2021, 11, 19).unwrap());
    }

    #[test]
    fn test_date_try_from() {
        let date = Date::try_from("2021-11-20");
        assert_that!(&date).is_ok();
        assert_that!(date.unwrap()).is_equal_to(&Date::new(2021, 11, 20).unwrap());
    }

    #[test]
    fn test_date_try_from_bad() {
        let date = Date::try_from("fred");
        assert_that!(&date).is_err();
    }

    #[test]
    fn test_datetime_new() {
        let date = DateTime::new((2021, 11, 20), (11, 32, 18));
        assert_that!(date).is_ok();
        assert_that!(date.unwrap().to_string()).is_equal_to(&String::from("2021-11-20 11:32:18"));
    }

    #[test]
    fn test_datetime_new_bad_date() {
        let date = DateTime::new((2021, 13, 20), (11, 32, 18));
        assert_that!(date).is_err();
    }

    #[test]
    fn test_datetime_new_bad_time() {
        let date = DateTime::new((2021, 11, 20), (11, 82, 18));
        assert_that!(date).is_err();
    }

    #[test]
    fn test_datetime_try_from() {
        let date = DateTime::try_from("2021-11-20 11:32:18");
        assert_that!(&date).is_ok();
        assert_that!(date)
            .is_ok()
            .is_equal_to(&DateTime::new((2021, 11, 20), (11, 32, 18)).unwrap());
    }

    #[test]
    fn test_datetime_sub() {
        let date = DateTime::new((2021, 11, 20), (11, 32, 18)).unwrap();
        let old = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        assert_that!(date.sub(&old))
            .is_ok()
            .is_equal_to(&Duration::from_secs(2 * 86400 - 28 * 60 + 18));
    }

    #[test]
    fn test_datetime_sub_bad() {
        let date = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        let old  = DateTime::new((2021, 11, 20), (11, 32, 18)).unwrap();
        assert_that!(date.sub(&old)).is_err();
    }

    #[test]
    fn test_datetime_add_time() {
        let date = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        let new = date.add(DateTime::minutes(10));
        assert_that!(new)
            .is_ok()
            .is_equal_to(&DateTime::new((2021, 11, 18), (12, 10, 00)).unwrap());
    }

    #[test]
    fn test_datetime_add_days() {
        let date = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        let new = date.add(DateTime::days(3));
        assert_that!(new)
            .is_ok()
            .is_equal_to(&DateTime::new((2021, 11, 21), (12, 00, 00)).unwrap());
    }

    #[test]
    fn test_datetime_try_from_bad() {
        let date = DateTime::try_from("fred");
        assert_that!(&date).is_err();
    }
}
