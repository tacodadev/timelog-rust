//! Represention of a day as a set of times, events, and durations.
//!
//! # Examples
//!
//! ```rust, no_run
//! use timelog::{Day, Event, Result};
//!
//! # fn main() -> Result<()> {
//! # let events: Vec<Event> = vec![];
//! # let mut event_iter = events.into_iter();
//! let mut day = Day::new("2021-07-02")?;
//! while let Some(event) = event_iter.next() {
//!     day.add_event(&event);
//! }
//! day.finish()?;
//! print!("{}", day.detail_report());
//! #   Ok(())
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Day`] type represents the events of a particular day. It tracks projects and combines time
//! spent on the same task from multiple points in the day.
//!
//! [`Day`] also provides the ability to print various reports on the day's
//! activities.

#[cfg(doc)]
use crate::date;
#[doc(inline)]
use crate::date::{Date, DateTime};
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::event::Event;
#[doc(inline)]
use crate::chart::{BarGraph, ColorIter, DayHours, Legend, Percentages, PieChart, PieData, TagPercent};
use crate::Result;
use crate::TaskEvent;
use crate::emit_xml;

use std::cmp::{Ordering, PartialOrd};
use std::collections::HashMap;
use std::fmt::{self, Display};
use std::time::Duration;
use std::io::Write;

use xml::writer::{EventWriter, XmlEvent};

use regex::Regex;

/// Type representing a day and all associated tasks
#[derive(Debug)]
pub struct Day {
    /// [`Date`] for today
    stamp: Date,
    /// Optional timestamp representing the start of events for the day
    start: Option<DateTime>,
    /// Total [`Duration`] of all of the events for the day
    dur: Duration,
    /// Map of tasks to task events
    tasks: HashMap<String, TaskEvent>,
    /// Map of projects to task events
    proj_dur: HashMap<String, Duration>,
    /// List of task events
    events: Vec<TaskEvent>,
    /// Optional start of most recent event while parsing a day
    last_start: Option<DateTime>,
    /// Optional most recent event
    last_event: Option<Event>,
}

/// Format duration information as a [`String`] of the form `M:SS`
pub fn format_dur(dur: &Duration) -> String {
    let secs = dur.as_secs() + 30; // force rounding
    format!("{:>2}:{:0>2}", (secs / 3600), (secs % 3600) / 60)
}

impl<'a> Day {
    /// Creates a [`Day`] struct that collects the events for the date specified by
    /// the `stamp`.
    ///
    /// ## Errors
    ///
    /// - Return an [`Error::MissingDate`] if the string is empty.
    /// - Return an [`InvalidDate`](date::Error::InvalidDate) error if the `stamp` is not formatted as 'YYYY-MM-DD'.
    pub fn new(stamp: &str) -> Result<Self> {
        if stamp.is_empty() {
            return Err(Error::MissingDate);
        }
        Ok(Day {
            stamp: Date::try_from(stamp)?,
            start: None,
            dur: Duration::default(),
            tasks: HashMap::new(),
            proj_dur: HashMap::new(),
            events: Vec::new(),
            last_start: None,
            last_event: None,
        })
    }

    /// Return the duration of the day in seconds
    pub fn duration_secs(&self) -> u64 {
        self.dur.as_secs()
    }

    /// Returns `true` only if no events have been added to the day.
    pub fn is_empty(&self) -> bool {
        self.duration_secs() == 0
    }

    /// Returns `true` only the day is complete.
    pub fn is_complete(&self) -> bool {
        self.last_start.is_none()
    }

    /// Return the date stamp for the day in 'YYYY-MM-DD' form.
    pub fn date_stamp(&self) -> String {
        self.stamp.into()
    }

    /// Return the date for the [`Day`] object in a [`Date`].
    pub fn date(&self) -> Date {
        self.stamp
    }

    /// Return an iterator over the project names for today
    pub fn projects(&self) -> impl Iterator<Item=&'_ str> {
        self.proj_dur.keys().map(|k| k.as_str())
    }

    // Update the task duration for most recent [`Event`]
    fn update_task_duration(&mut self, prev: &Event, dur: &Duration) {
        match self.tasks.get_mut(&prev.event_text()) {
            Some(task) => task.add_dur(*dur),
            None => {
                let task = TaskEvent::new(prev.date_time(), prev.project(), *dur);
                self.tasks.insert(prev.event_text(), task);
            },
        }
    }

    // Update the project duration for most recent event
    fn update_project_duration(&mut self, proj: &str, dur: &Duration) {
        match self.proj_dur.get_mut(proj) {
            Some(proj_dur) => { *proj_dur += *dur; },
            None => { self.proj_dur.insert(proj.to_owned(), *dur); },
        }
    }

    /// Add an [`Event`] to the current [`Day`].
    ///
    /// ## Errors
    ///
    /// - Return an [`EventOrder`](date::Error::EventOrder) error if the new event is before the previous event.
    pub fn add_event(&mut self, event: &Event) -> Result<()> {
        self.update_dur(&event.date_time())?;
        self.start_task(event);
        self.last_event = (!event.is_stop()).then(|| event.clone());
        Ok(())
    }

    /// Update the duration of the most recent task
    ///
    /// ## Errors
    ///
    /// - Return an [`EventOrder`](date::Error::EventOrder) error if the new event is before the previous event.
    pub fn update_dur(&mut self, date_time: &DateTime) -> Result<()> {
        if let Some(prev) = &self.last_event.clone() {
            let curr_dur = date_time.sub(&prev.date_time())?;
            if !prev.event_text().is_empty() {
                self.update_task_duration(prev, &curr_dur);
            }
            if let Some(prev_proj) = prev.project() {
                self.update_project_duration(&prev_proj, &curr_dur);
            }
            self.dur += curr_dur;
            if let Some(prev) = self.events.last_mut() {
                prev.add_dur(curr_dur);
            }
        }
        Ok(())
    }

    /// Update the duration of the most recent task, using the `last` variable.
    ///
    /// ## Errors
    ///
    /// - Return an [`EventOrder`](date::Error::EventOrder) error if the new event is before the previous event.
    pub fn close_day(&mut self) -> Result<()> {
        if !self.is_complete() {
            self.update_dur(&self.stamp.day_end())?;
            self.last_start = None;
        }

        Ok(())
    }

    /// Update the duration of the most recent task
    ///
    /// ## Errors
    ///
    /// - Return an [`EventOrder`](date::Error::EventOrder) error if the new event is before the previous event.
    pub fn finish(&mut self) -> Result<()> {
        if !self.is_complete() {
            self.update_dur(&DateTime::now())?;
            self.last_start = None;
        }

        Ok(())
    }

    /// Initialize a new task item in the day based on the [`Event`] object supplied in `event`.
    ///
    /// This method only starts a task if no previous matching task exists in the day.
    pub fn start_task(&mut self, event: &Event) {
        if event.is_stop() {
            self.last_start = None;
            return;
        }
        let task = event.event_text();
        self.last_start = Some(event.date_time());
        self.tasks.entry(task).or_insert_with(|| TaskEvent::new(
                event.date_time(),
                event.project(),
                Duration::default(),
        ));
        self.events.push(TaskEvent::new(
            event.date_time(),
            event.project(),
            Duration::default(),
        ));
    }

    // Format the stamp line to f with the supplied `separator`.
    fn _format_stamp_line(&self, f: &mut fmt::Formatter<'_>, sep: &str) -> fmt::Result {
        writeln!(f, "{}{} {}", self.date_stamp(), sep, format_dur(&self.dur))
    }

    // Format the project line to `f` with the supplied `separator`.
    fn _format_project_line(&self, f:&mut fmt::Formatter<'_>, proj: &str, dur: &Duration) -> fmt::Result {
        writeln!(f, "  {:<13}{}", proj, format_dur(dur))
    }

    // Format the task line to `f` with the supplied `separator`.
    fn _format_task_line(&self, f:&mut fmt::Formatter<'_>, task: &str, dur: &Duration) -> fmt::Result {
        match Event::task_breakdown(task) {
            (Some(task), Some(detail)) => writeln!(f, "    {:<20}{} ({})", task, format_dur(dur), detail),
            (Some(task), None) => writeln!(f, "    {:<20}{}", task, format_dur(dur)),
            (None, Some(detail)) => writeln!(f, "    {:<20}{}", detail, format_dur(dur)),
            _ => writeln!(f, "    {:<20}{}", "", format_dur(dur)),
        }
    }

    /// Return a [`DetailReport`] from the current [`Day`].
    pub fn detail_report(&'a self) -> DetailReport<'a> {
        DetailReport(self)
    }

    /// Return a [`SummaryReport`] from the current [`Day`].
    pub fn summary_report(&'a self) -> SummaryReport<'a> {
        SummaryReport(self)
    }

    /// Return a [`HoursReport`] from the current [`Day`].
    pub fn hours_report(&'a self) -> HoursReport<'a> {
        HoursReport(self)
    }

    /// Return a [`DailyChart`] from the current [`Day`].
    pub fn daily_chart(&'a self) -> DailyChart<'a> {
        DailyChart(self)
    }

    /// Return `true` if the day contains one or more tasks.
    pub fn has_tasks(&self) -> bool {
        !self.tasks.is_empty()
    }

    // Filter tasks by project, returning a HashMap.
    fn project_filtered_tasks(&self, filter: &Regex) -> HashMap<String, TaskEvent> {
        self.tasks
            .iter()
            .filter(|(_, t)| filter.is_match(&t.project()))
            .fold(HashMap::new(), |mut h, (k, t)| {
                h.insert(k.to_string(), t.clone());
                h
            })
    }

    // Return a [`HashMap`] of projects and [`Duration`]s that match the supplied [`Regex`]es.
    fn project_filtered_durs(&self, filter: &Regex) -> HashMap<String, Duration> {
        self.proj_dur
            .iter()
            .filter(|(k, _)| filter.is_match(k))
            .fold(HashMap::new(), |mut h, (k, v)| {
                h.insert(k.to_string(), *v);
                h
            })
    }

    /// Make a copy of the current [`Day`] object containing only the tasks associated
    /// with a supplied [`Regex`].
    pub fn filtered_by_project(&self, filter: &Regex) -> Self {
        let proj_durs = self.project_filtered_durs(filter);
        Self {
            stamp: self.stamp,
            start: self.start,
            dur: proj_durs.values().sum(),
            tasks: self.project_filtered_tasks(filter),
            events: self.events.clone(),  // TODO: Need to filter somehow
            proj_dur: proj_durs,
            last_start: self.start,
            last_event: None,
        }
    }

    /// Return a [`Vec`] of tuples mapping project name to percentage of the overall
    /// time this project took.
    pub fn project_percentages(&'a self) -> Percentages {
        let mut pie = PieData::new();

        self.proj_dur.iter().for_each(|(proj, dur)| {
            pie.add_secs(proj.as_str(), dur.as_secs())
        });

        pie.percentages()
    }

    /// Return a [`Vec`] of tuples mapping the task name and percentage of the
    /// supplied project.
    pub fn task_percentages(&self, proj: &str) -> Percentages {
        let mut pie = PieData::new();

        self.tasks
            .iter()
            .filter(|(_t, tsk)| tsk.project() == proj)
            .for_each(|(t, tsk)| {
                let task = match Event::task_breakdown(t) {
                    (None, None) => String::new(),
                    (Some(tname), None) => tname.to_string(),
                    (None, Some(detail)) => format!(" ({})", detail),
                    (Some(tname), Some(detail)) => format!("{} ({})", tname, detail),
                };
                pie.add_secs(&task, tsk.as_secs());
            });

        pie.percentages()
    }

    // Return an iterator over the [`TaskEvents`] in the day.
    fn events(&self) -> impl Iterator<Item=&'_ TaskEvent> {
        self.events.iter()
    }
}

/// Representation of the full report about a [`Day`].
pub struct DetailReport<'a>(&'a Day);

impl<'a> Display for DetailReport<'a> {
    /// Format the [`Day`] information.
    ///
    /// The output starts with the current datestamp and duration for the day. Indented
    /// under that are individual projects. Individual tasks are indented under the
    /// projects.
    ///
    /// This is the most detailed report.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let day = self.0;
        let mut last_proj = String::new();
        writeln!(f)?;
        day._format_stamp_line(f, "")?;

        let mut tasks: Vec<(String, &String, &TaskEvent)> = day
            .tasks
            .iter()
            .map(|(t, tsk)| (tsk.project(), t, tsk))
            .collect();
        tasks.sort();

        for (cur_proj, tname, task) in tasks {
            if cur_proj != last_proj {
                day._format_project_line(
                    f,
                    &cur_proj,
                    day.proj_dur
                        .get(&cur_proj)
                        .unwrap_or(&Duration::default()),
                )?;
                last_proj = cur_proj
            }
            day._format_task_line(f, tname, &task.duration())?;
        }
        Ok(())
    }
}

/// Representation of the summary report about a [`Day`].
pub struct SummaryReport<'a>(&'a Day);

impl<'a> Display for SummaryReport<'a> {
    /// Format the [`Day`] information.
    ///
    /// The output starts with the current datestamp and duration for the day. Indented
    /// under that are individual projects.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let day = self.0;

        let proj_dur = &day.proj_dur;
        let mut keys: Vec<&String> = proj_dur.keys().collect();
        keys.sort();

        day._format_stamp_line(f, "")?;
        for proj in keys {
            day._format_project_line(f, proj, proj_dur.get(proj).unwrap())?;
        }
        Ok(())
    }
}

/// Representation of the hours report about a [`Day`].
pub struct HoursReport<'a>(&'a Day);

impl<'a> Display for HoursReport<'a> {
    /// Format the [`Day`] information.
    ///
    /// The output only displays the current datestamp and duration for the day.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0._format_stamp_line(f, ":")
    }
}

/// Representation of chart report about a [`Day`].
pub struct DailyChart<'a>(&'a Day);

impl<'a> DailyChart<'a> {
    /// Return a [`Vec`] of tuples mapping project name to percentage of the overall
    /// time this project took.
    pub fn project_percentages(&self) -> Percentages {
        self.0.project_percentages()
    }

    /// Return a [`Vec`] of tuples mapping the task name and percentage of the
    /// supplied project.
    pub fn task_percentages(&self, proj: &str) -> Percentages {
        self.0.task_percentages(proj)
    }

    /// Write a pie chart representing the projects for the current day to the supplied
    /// [`EventWriter`].
    pub fn project_pie<W: Write>(&self, w: &mut EventWriter<W>) -> Result<()> {
        const R: f32 = 100.0;
        let legend = Legend::new(14.0, ColorIter::default());
        let pie = PieChart::new(R, legend);

        let mut percents = self.0.project_percentages();
        percents.sort_by(|a, b| a.partial_cmp(&b).unwrap_or(Ordering::Equal));
        emit_xml!(w, div, class: "project" => {
            emit_xml!(w, h2; &format!("{} ({}) Projects", self.0.date_stamp(), self.0.date().weekday()))?;
            pie.write_pie(w, &percents)?;
            self.project_hours(w)
        })
    }

    /// Write a bar-graph representation of the tasks by hour in the current day to the supplied
    /// [`EventWriter`].
    pub fn project_hours<W: Write>(&self, w: &mut EventWriter<W>) -> Result<()> {
        emit_xml!(w, div, class: "hours" => {
            emit_xml!(w, h3; "Hourly")?;
            emit_xml!(w, div, class: "hist" => {
                let mut day_hours = DayHours::new();
                for event in self.0.events() {
                    day_hours.add(event.clone());
                }
                let bar_graph = BarGraph::new(&self.0.project_percentages());
                bar_graph.write(w, &day_hours)
            })
        })
    }

    /// Write a pie chart representing the tasks for the supplied project to the supplied
    /// [`EventWriter`].
    pub fn task_pie<W: Write>(&self, w: &mut EventWriter<W>, proj: &str, percent: f32) -> Result<()> {
        const R: f32 = 60.0;
        let legend = Legend::new(12.0, ColorIter::default());
        let pie = PieChart::new(R, legend);

        let mut percents = self.0.task_percentages(proj);
        percents.sort_by(|a, b| a.partial_cmp(&b).unwrap_or(Ordering::Equal));
        emit_xml!(w, div => {
            emit_xml!(w, h3 => {
                emit_xml!(w; "Tasks for ")?;
                emit_xml!(w, em; &format!("{} ({})", proj, TagPercent::fmt_percent(percent)))
            })?;
            pie.write_pie(w, &percents)
        })
    }

    /// Write the charts for the current day to the supplied [`EventWriter`].
    pub fn write<W: Write>(&self, w: &mut EventWriter<W>) -> Result<()> {
        emit_xml!(w, div, class: "day" => {
            self.project_pie(w)?;
            emit_xml!(w, div, class: "tasks" => {
                let mut percentages = self.0.project_percentages();
                percentages.sort_by(|a, b| a.partial_cmp(&b).unwrap_or(Ordering::Equal));
                for percent in percentages {
                    self.task_pie(w, percent.label(), percent.percent())?;
                }
                Ok(())
            })
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::event::Event;
    use crate::chart::TagPercent;
    use spectral::prelude::*;

    const INITIAL_EVENTS: [(&str, u64); 8] = [
        ("+proj1 @Make changes", 0),
        ("+proj2 @Start work", 1),
        ("+proj1 @Make changes", 2),
        ("+proj1 @Stuff Other changes", 3),
        ("stop", 4),
        ("+proj1 @Stuff Other changes", 4),
        ("+proj1 @Final", 5),
        ("stop", 6),
    ];
    const MORE_EVENTS: [(&str, u64); 4] = [
        ("+proj3 @Phone call", 60 + 0),
        ("+proj4 @Research", 60 + 1),
        ("+proj3 @Phone call", 60 + 2),
        ("stop", 60 + 4),
    ];

    #[test]
    fn test_new_empty_stamp() {
        assert_that!(Day::new("")).is_err_containing(Error::MissingDate);
    }

    #[test]
    fn test_new_invalid_stamp() {
        assert_that!(Day::new("foo")).is_err_containing(&(crate::date::Error::InvalidDate).into());
    }

    #[test]
    fn test_update_dur() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        let _ = day.update_dur(&DateTime::new((2021, 6, 10), (8, 0, 0)).unwrap());
        assert_that!(day.duration_secs()).is_equal_to(&0);
    }

    #[test]
    fn test_add_event() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        let event = Event::from_line("2021-06-10 08:00:00 +proj1 do something").unwrap();
        let _ = day.add_event(&event);
        let event = Event::from_line("2021-06-10 08:45:00 stop").unwrap();
        let _ = day.add_event(&event);
        assert_that!(day.duration_secs()).is_equal_to(&(45 * 60));
    }

    #[test]
    fn test_format_dur() {
        assert_that!(format_dur(&Duration::default())).is_equal_to(&String::from(" 0:00"));
        assert_that!(format_dur(&Duration::from_secs(3600))).is_equal_to(&String::from(" 1:00"));
        assert_that!(format_dur(&Duration::from_secs(3629))).is_equal_to(&String::from(" 1:00"));
        assert_that!(format_dur(&Duration::from_secs(3630))).is_equal_to(&String::from(" 1:01"));
        assert_that!(format_dur(&Duration::from_secs(3660))).is_equal_to(&String::from(" 1:01"));
        assert_that!(format_dur(&Duration::from_secs(36000))).is_equal_to(&String::from("10:00"));
        assert_that!(format_dur(&Duration::from_secs(360000))).is_equal_to(&String::from("100:00"));
    }

    #[test]
    fn test_new_empty() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let day = day_result.unwrap();
        assert_that!(&day.is_empty()).is_true();
        assert_that!(&day.duration_secs()).is_equal_to(0u64);
        assert_that!(&day.is_complete()).is_true();
        assert_that!(&day.date_stamp()).is_equal_to(&String::from("2021-06-10"));
    }

    #[test]
    fn test_detail_report_empty() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let day = day_result.unwrap();
        let expect = String::from("\n2021-06-10  0:00\n");
        assert_that!(format!("{}", day.detail_report())).is_equal_to(expect);
    }

    #[test]
    fn test_summary_report_empty() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let day = day_result.unwrap();
        let detail = format!("{}", day.summary_report());
        let expected = String::from("2021-06-10  0:00\n");
        assert_that!(detail).is_equal_to(expected);
    }

    #[test]
    fn test_hours_report_empty() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let day = day_result.unwrap();
        let expect = String::from("2021-06-10:  0:00\n");
        assert_that!(format!("{}", day.hours_report())).is_equal_to(expect);
    }

    #[test]
    fn test_detail_report_with_one() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        let event = Event::from_line("2021-06-10 08:00:00 +foo @task").unwrap();
        let _ = day.add_event(&event);
        let stamp = event.date_time();
        let _ = day.update_dur(&stamp.add(DateTime::minutes(45)).unwrap());
        let expect = String::from(
            "\n2021-06-10  0:45\n  foo           0:45\n    task                 0:45\n",
        );
        assert_that!(format!("{}", day.detail_report())).is_equal_to(expect);
    }

    fn add_events(day: &mut Day) -> Result<()> {
        add_some_events(
            day,
            DateTime::new((2021, 6, 10), (8, 0, 0)).unwrap(),
            INITIAL_EVENTS.iter()
        )?;
        day.finish()?;
        Ok(())
    }

    fn add_extra_events(day: &mut Day) -> Result<()> {
        add_some_events(
            day,
            DateTime::new((2021, 6, 10), (8, 0, 0)).unwrap(),
            INITIAL_EVENTS.iter().chain(MORE_EVENTS.iter())
        )?;
        day.finish()?;
        Ok(())
    }

    fn add_some_events<'b, I>(day: &mut Day, stamp: DateTime, events: I) -> Result<()>
    where I: Iterator<Item=&'b (&'b str, u64)> {
        for (event, mins) in events {
            let ev = Event::new(event, stamp.add(DateTime::minutes(*mins)).unwrap());
            day.add_event(&ev)?;
        }
        Ok(())
    }

    #[test]
    fn test_detail_report_tasks() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_events(&mut day).expect("Events out of order");

        let lines = [
            "\n",
            "2021-06-10  0:06\n",
            "  proj1         0:05\n",
            "    Final                0:01\n",
            "    Make                 0:02 (changes)\n",
            "    Stuff                0:02 (Other changes)\n",
            "  proj2         0:01\n",
            "    Start                0:01 (work)\n",
        ];
        let expect = lines.iter().fold(String::new(), |mut acc, s| {
            acc.push_str(s);
            acc
        });
        assert_that!(format!("{}", day.detail_report())).is_equal_to(expect);
    }

    #[test]
    fn test_summary_report_tasks() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_events(&mut day).expect("Events out of order");

        let lines = [
            "2021-06-10  0:06\n",
            "  proj1         0:05\n",
            "  proj2         0:01\n",
        ];

        let expected = lines.iter().fold(String::new(), |mut acc, s| {
            acc.push_str(s);
            acc
        });
        assert_that!(format!("{}", day.summary_report())).is_equal_to(expected);
    }

    #[test]
    fn test_project_percentages() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_extra_events(&mut day).expect("Events out of order");

        let expect: Percentages = vec![
            TagPercent::new("proj1", 50.0).unwrap(),
            TagPercent::new("proj3", 30.0).unwrap(),
            TagPercent::new("proj2", 10.0).unwrap(),
            TagPercent::new("proj4", 10.0).unwrap(),
        ];
        let mut actual = day.daily_chart().project_percentages();
        actual.sort_by(|lhs, rhs| lhs.partial_cmp(rhs).unwrap());
        assert_that!(actual).is_equal_to(expect);
    }

    #[test]
    fn test_task_percentages() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_extra_events(&mut day).expect("Events out of order");

        let expect: Percentages = vec![
            TagPercent::new("Make (changes)", 40.0).unwrap(),
            TagPercent::new("Stuff (Other changes)", 40.0).unwrap(),
            TagPercent::new("Final", 20.0).unwrap(),
        ];
        let mut actual = day.task_percentages("proj1");
        actual.sort_by(|lhs, rhs| lhs.partial_cmp(rhs).unwrap());
        assert_that!(actual).is_equal_to(expect);
    }

    #[test]
    fn test_hours_report_tasks() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_events(&mut day).expect("Events out of order");
        let expect = String::from("2021-06-10:  0:06\n");
        assert_that!(format!("{}", day.hours_report())).is_equal_to(expect);
    }

    #[test]
    fn test_project_filter_regex() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_events(&mut day).expect("Events out of order");
        let regex = Regex::new(r"^\w+1$").expect("Invalid project regex");
        let day2 = day.filtered_by_project(&regex);

        assert_that!(day2.proj_dur.len()).is_equal_to(&1);
        assert_that!(day2.proj_dur.contains_key("proj1")).is_true();

        let expected = String::from("2021-06-10  0:05\n  proj1         0:05\n");
        assert_that!(format!("{}", day2.summary_report())).is_equal_to(expected);
    }

    #[test]
    fn test_events() {
        let day_result = Day::new("2021-06-10");
        assert_that!(&day_result).is_ok();

        let mut day = day_result.unwrap();
        add_extra_events(&mut day).expect("Events out of order");

        assert_that!(day.events().count()).is_equal_to(9);
        let expected = [
            (DateTime::new((2021, 6, 10), (8, 0, 0)).unwrap(), "proj1",  60),
            (DateTime::new((2021, 6, 10), (8, 1, 0)).unwrap(), "proj2",  60),
            (DateTime::new((2021, 6, 10), (8, 2, 0)).unwrap(), "proj1",  60),
            (DateTime::new((2021, 6, 10), (8, 3, 0)).unwrap(), "proj1",  60),
            (DateTime::new((2021, 6, 10), (8, 4, 0)).unwrap(), "proj1",  60),
            (DateTime::new((2021, 6, 10), (8, 5, 0)).unwrap(), "proj1",  60),
            (DateTime::new((2021, 6, 10), (9, 0, 0)).unwrap(), "proj3",  60),
            (DateTime::new((2021, 6, 10), (9, 1, 0)).unwrap(), "proj4",  60),
            (DateTime::new((2021, 6, 10), (9, 2, 0)).unwrap(), "proj3", 120),
        ];
        for (ev, expect) in day.events().zip(expected.iter()) {
            assert_that!(ev.start()).is_equal_to(&expect.0);
            assert_that!(ev.proj()).contains(&expect.1.to_string());
            assert_that!(ev.as_secs()).is_equal_to(expect.2);
        }
    }
}
