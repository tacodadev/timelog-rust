//! An error that occurs in working with timelogs
use thiserror::Error;

use crate::date;

/// Errors encountered accessing files and directories.
#[derive(Error, Debug, PartialEq)]
pub enum PathError {
    /// Error accessing a file when reading. File is first string, error message is second.
    #[error("Error accessing file '{0}': {1}")]
    FileAccess(String, String),

    /// Error writing to a file. File is first string, error message is second.
    #[error("Error writing to file '{0}': {1}")]
    FileWrite(String, String),

    /// Required filename not supplied.
    #[error("Required filename not supplied")]
    FilenameMissing,

    /// Attempt to access a file in directory that is not available. Directory is first string, error message is second.
    #[error("Missing directory '{0}': {1}")]
    InvalidPath(String, String),

    /// Invalid path for the logfiles.
    #[error("Logfiles path contains invalid characters")]
    InvalidTimelogPath,

    /// Invalid path for the logfiles.
    #[error("Config path contains invalid characters")]
    InvalidConfigPath,

    /// The filename references an existing file.
    #[error("Cannot overwrite, '{0}' already exists")]
    AlreadyExists(String),

    /// The filename references an existing file. File is first string, error message is second.
    #[error("Cannot rename '{0}': {1}")]
    RenameFailure(String, String),

    /// Cannot create path. Path is first string, error message is second.
    #[error("Cannot create path '{0}': {1}")]
    CantCreatePath(String, String),

    /// Cannot read timelog
    #[error("Cannot read timelog")]
    CantReadTimelog,

    /// Cannot write to report file
    #[error("Cannot write report file")]
    CantWriteReport,
}

/// Enumeration of errors that can happen processing timelogs
#[derive(Error, Debug, PartialEq)]
pub enum Error {
    /// Not a validly formatted event line.
    #[error("Not a valid event line.")]
    InvalidEventLine,

    /// Event line is missing the required stamp.
    #[error("Missing required stamp.")]
    MissingDate,

    /// Invalid starting date in a date pair.
    #[error("Invalid starting date.")]
    StartDateFormat,

    /// Invalid ending date in a date pair.
    #[error("Invalid ending date.")]
    EndDateFormat,

    /// Start date after end date.
    #[error("Start date after end date.")]
    WrongDateOrder,

    /// Unable to pop stack item.
    #[error("Unable to pop stack item")]
    StackPop,

    /// Invalid drop argument
    #[error("Invalid drop argument '{0}'")]
    InvalidDrop(String),

    /// Project descriptors invalid
    #[error("Bad project filters")]
    BadProjectFilter,

    /// Not a valid timelog command
    #[error("'{0}' is not a valid command")]
    InvalidCommand(String),

    /// Failed to execute editor on logfile. File is first string, error message is second.
    #[error("Editor '{0}' failed to execute: {1}")]
    EditorFailed(String, String),

    /// Errors in path/file handling
    #[error(transparent)]
    DateError {
        #[from]
        source: date::Error,
    },

    /// Errors in path/file handling
    #[error(transparent)]
    PathError {
        #[from]
        source: PathError,
    },
}

impl From<xml::writer::Error> for Error {
    /// Conversion from an [`xml::writer::Error`] to a timelog [`enum@Error`].
    fn from(_e: xml::writer::Error) -> Error {
        PathError::CantWriteReport.into()
    }
}
