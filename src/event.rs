//! Module representing an event in the timelog.
//!
//! # Examples
//!
//! ```rust
//! use timelog::event::Event;
//! use std::fs::File;
//! use std::io::{BufRead, BufReader};
//!
//! fn day_events(date: &str, file: &mut File) -> Vec<Event> {
//!     let mut reader = BufReader::new(file);
//!     reader.lines()
//!           .filter_map(|line| Event::from_line(&line.ok()?).ok())
//!           .filter(|ev| ev.stamp() == String::from(date))
//!           .collect::<Vec<Event>>()
//! }
//! ```
//!
//! # Description
//!
//! Objects of this type represent the individual lines in the `timelog.txt` file.
//! Each [`Event`] has a date and time stamp, an optional project, and a task.

use lazy_static::lazy_static;
use regex::Regex;
use std::fmt::{self, Debug, Display};

const STOP_CMD: &str = "stop";

lazy_static! {
    // These should not be able to fail, hardcoded input strings.
    // Still using expect() in case the regex strings ever get changed.

    /// A somewhat lax regular expression to match an event line.
    static ref LAX_LINE_RE: Regex = Regex::new(r"(\d{4}[-/](?:0[1-9]|1[0-2])[-/](?:0[1-9]|[12][0-9]|3[01]) (?:[01][0-9]|2[0-3]):[0-5][0-9]:[0-6][0-9]) (.*)").expect("Event line Regex failed.");
    /// A regular expression matching the project part of an event line.
    static ref PROJECT_RE: Regex = Regex::new(r"\+(\S+)").expect("Event project regex failed.");
    /// A regular expression matching the task part of an event line.
    static ref TASK_RE: Regex = Regex::new(r"@(\S+)").expect("Task Regex failed.");
}

#[doc(inline)]
use crate::date::{Date, DateTime};
#[doc(inline)]
use crate::error::Error;
use crate::Result;

/// Representation of an event in the log
///
/// Objects of this type represent individual lines in the `timelog.txt` file.
/// Each [`Event`] has a date and time stamp, an optional project, and a task.
#[derive(Debug, Clone, PartialEq)]
pub struct Event {
    /// Time that this event began
    time: DateTime,
    /// An optional project name
    project: Option<String>,
    /// The text of the event from the event line
    text: String,
}

impl Event {
    /// Create a new [`Event`] representing the supplied task at the supplied time.
    pub fn new(event_text: &str, time: DateTime) -> Self {
        let oproject = PROJECT_RE.captures(event_text).map(|caps| {
            caps.get(1).map(|m| String::from(m.as_str()))
        }).flatten();
        Self { time, project: oproject, text: String::from(event_text) }
    }

    /// Create a new [`Event`] representing the event from the supplied line.
    ///
    /// This event must be formatted as described in Format.md.
    ///
    /// ## Errors
    ///
    /// Return an [`Error::InvalidEventLine`] if the line is empty or formatted incorrectly.
    pub fn from_line(line: &str) -> Result<Self> {
        if line.is_empty() {
            return Err(Error::InvalidEventLine);
        }

        let caps = LAX_LINE_RE.captures(line).ok_or(Error::InvalidEventLine)?;
        let stamp = caps.get(1).ok_or(Error::InvalidEventLine)?.as_str();
        let time = DateTime::try_from(stamp)?;
        Ok(Event::new(
            caps.get(2).map(|m| m.as_str()).unwrap_or(""),
            time,
        ))
    }

    /// Parse the task line into the short task and detail parts if they exist.
    pub fn task_breakdown(event_text: &str) -> (Option<String>, Option<String>) {
        if event_text.is_empty() {
            return (None, None);
        }

        let task = PROJECT_RE.replace(&event_text, "").trim().to_string();
        if let Some(caps) = TASK_RE.captures(&task) {
            if let Some(tname) = caps.get(1) {
                let detail = TASK_RE.replace(&task, "").trim().to_string();
                let tname = tname.as_str().to_string();
                return (Some(tname), (!detail.is_empty()).then(|| detail));
            }
        }
        (None, (!task.is_empty()).then(|| task))
    }

    /// Return the [`String`] designated as the project, if any, from the [`Event`].
    pub fn project(&self) -> Option<String> {
        self.project.as_ref().map(|p| p.to_owned())
    }

    /// Return the [`String`] containing all of the [`Event`] except the time and date.
    pub fn event_text(&self) -> String {
        self.text.to_owned()
    }

    /// Return the [`String`] containing all of the [`Event`] except the time and date.
    pub fn task(&self) -> Option<String> {
        Self::task_breakdown(&self.text).0
    }

    /// Return the [`String`] containing all of the [`Event`] except the time and date.
    pub fn detail(&self) -> Option<String> {
        Self::task_breakdown(&self.text).1
    }

    /// Return the [`String`] containing all of the [`Event`] except the time and date.
    pub fn task_and_detail(&self) -> (Option<String>, Option<String>) {
       Self::task_breakdown(&self.text)
    }

    /// Return the time for the start of the [`Event`] in epoch seconds.
    pub fn epoch(&self) -> i64 {
        self.time.timestamp()
    }

    /// Return the date for the start of the [`Event`] as a [`Date`]
    pub fn date(&self) -> Date {
        self.time.date()
    }

    /// Return the time for the start of the [`Event`] as a [`DateTime`]
    pub fn date_time(&self) -> DateTime {
        self.time
    }

    /// Return the date stamp of the [`Event`] in 'YYYY-MM-DD' format.
    pub fn stamp(&self) -> String {
        self.date().to_string()
    }

    /// Return `true` if this was a stop [`Event`].
    pub fn is_stop(&self) -> bool {
        self.text.as_str() == STOP_CMD
    }
}

impl Display for Event {
    /// Format the [`Event`] formatted as described in Format.md.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.time, self.text)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    const CANONICAL_LINE: &str = "2013-06-05 10:00:02 +proj1 @do something";
    const STOP_LINE: &str = "2013-06-05 10:00:02 stop";

    fn reference_time() -> i64 {
        DateTime::new((2013,  6, 5), (10, 0, 2)).unwrap().timestamp()
    }

    #[test]
    fn from_line_error_if_empty() {
        assert_that!(Event::from_line("")).is_err_containing(Error::InvalidEventLine);
    }

    #[test]
    fn from_line_error_if_not_event() {
        assert_that!(Event::from_line("This is not an event"))
            .is_err_containing(Error::InvalidEventLine);
    }

    #[test]
    fn from_line_canonical_event() {
        let event = Event::from_line(CANONICAL_LINE).unwrap();
        assert_that!(&event.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&event.project()).contains_value(String::from("proj1"));
        assert_that!(&event.event_text()).is_equal_to(String::from("+proj1 @do something"));
        assert_that!(&event.task()).contains_value(String::from("do"));
        assert_that!(&event.detail()).contains_value(String::from("something"));
        assert_that!(&event.task_and_detail()).is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&CANONICAL_LINE);
        assert_that!(&event.is_stop()).is_false();
    }

    #[test]
    fn new_canonical_event() {
        let canonical_time = DateTime::try_from("2013-06-05 10:00:02").unwrap();
        let event = Event::new("+proj1 @do something", canonical_time);
        assert_that!(&event.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&event.project()).contains_value(String::from("proj1"));
        assert_that!(&event.event_text()).is_equal_to(String::from("+proj1 @do something"));
        assert_that!(&event.task()).contains_value(String::from("do"));
        assert_that!(&event.detail()).contains_value(String::from("something"));
        assert_that!(&event.task_and_detail()).is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&CANONICAL_LINE);
        assert_that!(&event.is_stop()).is_false();
    }

    #[test]
    fn from_line_no_task_event() {
        const LINE: &str = "2013-06-05 10:00:02 +proj1 do something";
        let event = Event::from_line(LINE).unwrap();
        assert_that!(&event.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&event.project()).contains_value(String::from("proj1"));
        assert_that!(&event.event_text()).is_equal_to(String::from("+proj1 do something"));
        assert_that!(&event.task()).is_none();
        assert_that!(&event.detail()).contains_value(String::from("do something"));
        assert_that!(&event.task_and_detail()).is_equal_to((None, Some(String::from("do something"))));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&LINE);
        assert_that!(&event.is_stop()).is_false();
    }

    #[test]
    fn from_line_no_detail_event() {
        const LINE: &str = "2013-06-05 10:00:02 +proj1 @something";
        let event = Event::from_line(LINE).unwrap();
        assert_that!(&event.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&event.project()).contains_value(String::from("proj1"));
        assert_that!(&event.event_text()).is_equal_to(String::from("+proj1 @something"));
        assert_that!(&event.task()).contains_value(String::from("something"));
        assert_that!(&event.detail()).is_none();
        assert_that!(&event.task_and_detail()).is_equal_to((Some(String::from("something")), None));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&LINE);
        assert_that!(&event.is_stop()).is_false();
    }

    #[test]
    fn from_line_no_event_text() {
        const LINE: &str = "2013-06-05 10:00:02 +proj1";
        let event = Event::from_line(LINE).unwrap();
        assert_that!(&event.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&event.project()).contains_value(String::from("proj1"));
        assert_that!(&event.event_text()).is_equal_to(String::from("+proj1"));
        assert_that!(&event.task()).is_none();
        assert_that!(&event.detail()).is_none();
        assert_that!(&event.task_and_detail()).is_equal_to((None, None));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&LINE);
        assert_that!(&event.is_stop()).is_false();
    }

    #[test]
    fn from_line_stop_event() {
        let event = Event::from_line(STOP_LINE).unwrap();
        assert_that!(&event.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&event.project()).is_none();
        assert_that!(&event.event_text()).is_equal_to(String::from("stop"));
        assert_that!(&event.task()).is_none();
        assert_that!(&event.detail()).contains_value(String::from("stop"));
        assert_that!(&event.task_and_detail()).is_equal_to((None, Some(String::from("stop"))));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&STOP_LINE);
        assert_that!(&event.is_stop()).is_true();
    }

    #[test]
    fn new_stop_event() {
        let canonical_time = DateTime::try_from("2013-06-05 10:00:02").unwrap();
        let event = Event::new("stop", canonical_time);
        assert_that!(&event.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&event.project()).is_none();
        assert_that!(&event.event_text()).is_equal_to(String::from("stop"));
        assert_that!(&event.task()).is_none();
        assert_that!(&event.detail()).contains_value(String::from("stop"));
        assert_that!(&event.task_and_detail()).is_equal_to((None, Some(String::from("stop"))));
        assert_that!(&event.epoch()).is_equal_to(&reference_time());
        assert_that!(&event.to_string().as_str()).is_equal_to(&STOP_LINE);
        assert_that!(&event.is_stop()).is_true();
    }
}
