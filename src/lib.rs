//! System for logging time events in a text-log-based format.
//!
//! This library supports the various concepts that go into making a timelog.
//! The core functionality is based around the concepts:
//!
//! - [`Day`](day::Day) - a container for events that we wish to track
//! - [`Event`](event::Event) - a task to be accomplished as a continuous chunk of time
//! - [`Logfile`](logfile::Logfile) - list of all the that events started by the program
//! - [`Stack`](stack::Stack) - a stack of tasks that we may want to go back to
//!
//! Further support for working these events is supplied by:
//!
//! - [`Cli`](cli::Cli) - Handles the functionality provided by the command line tool
//! - [`Config`](config::Config) - Wrap the configuration information in an object
//! - [`Date`](date::Date) - A utility type that simplifies working with dates (including parsing, etc.)
//! - [`DateTime`](date::DateTime) - A utility type that simplifies working with date/times (including parsing, etc.)
//! - [`Error`](error::Error) - an enumeration of the errors that can be encountered in processing timelogs
//! - [`Result`] - Result specialized for [`Error`](error::Error)
//! - [`TaskEvent`](task::TaskEvent) - Type representing a single event tracked by timelog.
//! - [`TaskLineIter`](task_line_iter::TaskLineIter) - an iterator for walking the event lines in a timelog file

extern crate chrono;
extern crate configparser;
extern crate getset;
extern crate regex;
extern crate structopt;
extern crate tilde_expand;
extern crate xml;

#[cfg(test)]
extern crate spectral;

#[cfg(test)]
extern crate tempfile;

pub(crate) mod archive;
pub mod chart;
pub mod cli;
pub mod config;
pub mod date;
pub mod day;
pub mod error;
pub mod event;
pub mod logfile;
pub(crate) mod macros;
pub mod stack;
pub mod task;
pub mod task_line_iter;

/// Command line parser and application driver
pub type Cli = cli::Cli;

/// Wrapper for configuration information
pub type Config = config::Config;

/// Wrapper for Dates as used in the crate
pub type Date = date::Date;

/// Wrapper for Date/Times as used in the crate
pub type DateTime = date::DateTime;

/// Represention of a day as a set of times, events, and durations.
pub type Day = day::Day;

/// An error that occurs in the working with timelogs
pub type Error = error::Error;

/// Module representing an event in the timelog
pub type Event = event::Event;

/// Result type for timelog
pub type Result<T> = std::result::Result<T, Error>;

/// Interface to the stack file for the timelog application.
pub type Stack = stack::Stack;

/// Interface to the logfile for the timelog application.
pub type Logfile = logfile::Logfile;

/// Type representing a single task event.
pub type TaskEvent = task::TaskEvent;

/// Iterator for walking task lines
pub type TaskLineIter<'a, I> = task_line_iter::TaskLineIter<'a, I>;
