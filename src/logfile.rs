//! Interface to the timelog file for the timelog application.
//!
//! # Examples
//!
//! ```rust
//! use timelog::logfile::Logfile;
//! # fn main() -> Result<(), timelog::Error> {
//! let timelog = Logfile::new("./timelog.txt" )?;
//!
//! let task = timelog.last_line();
//! println!("{:?}", task);
//!
//! timelog.add_task("+Project @Task More detail");
//! #   std::fs::remove_file(timelog.clone_file()).expect("Oops");
//! #   Ok(())
//! # }
//! ```

use std::fs::{canonicalize, File, OpenOptions};
use std::io::prelude::*;
use std::io::{BufRead, BufReader, BufWriter};
use std::path::{Path, PathBuf};

#[doc(inline)]
use crate::date::DateTime;
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;
#[doc(inline)]
use crate::event::Event;

/// A [`Logfile`] type that wraps the timelog log file.
#[derive(Debug)]
pub struct Logfile(String);

impl Logfile {
    /// Creates a [`Logfile`] object wrapping the supplied file.
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
    /// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
    pub fn new(file: &str) -> std::result::Result<Self, PathError> {
        if file.is_empty() {
            return Err(PathError::FilenameMissing);
        }
        let mut dir = PathBuf::from(file);
        let filename = dir
            .file_name()
            .ok_or(PathError::FilenameMissing)?
            .to_os_string();
        dir.pop();

        let mut candir = canonicalize(dir)
            .map_err(|e| PathError::InvalidPath(file.to_owned(), e.to_string()))?;
        candir.push(filename);

        Ok(Self(candir.to_str().unwrap().to_owned()))
    }

    /// Open the log file, return a [`File`].
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if unable to open the file.
    pub fn open(&self) -> std::result::Result<File, PathError> {
        File::open(&self.0).map_err(|e| PathError::FileAccess(self.0.to_owned(), e.to_string()))
    }

    /// Clone the filename
    pub fn clone_file(&self) -> String {
        self.0.to_owned()
    }

    /// Return `true` if the timelog file exists
    pub fn exists(&self) -> bool {
        Path::new(&self.0).exists()
    }

    /// Append the supplied line (including time stamp) to the timelog file
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_line(&self, event: &str) -> std::result::Result<(), PathError> {
        let file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(&self.0)
            .map_err(|e| PathError::FileAccess(self.clone_file(), e.to_string()))?;
        let mut stream = BufWriter::new(file);
        writeln!(&mut stream, "{}", event)
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        Ok(())
    }

    /// Append the supplied task to the timelog file
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_task(&self, task: &str) -> std::result::Result<(), PathError> {
        self.add_event(&Event::new(task, DateTime::now()))
    }

    /// Append the supplied [`Event`] to the timelog file
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_event(&self, event: &Event) -> std::result::Result<(), PathError> {
        let line = format!("{}", event);
        self.add_line(&line)
    }

    /// Return the last line of the timelog file or `None` if we can't.
    pub fn last_line(&self) -> Option<String> {
        if self.exists() {
            let file = File::open(&self.0).ok()?;
            BufReader::new(file)
                .lines()
                .take_while(|ol| ol.is_ok())
                .last()
                .map(|ol| ol.unwrap())
        }
        else {
            None
        }
    }

    /// Return the last line of the timelog file as an [`Event'].
    ///
    /// ## Errors
    ///
    /// - Return [`Error::InvalidEventLine`] if the line is not correctly formatted.
    pub fn last_event(&self) -> std::result::Result<Event, Error> {
        Event::from_line(&self.last_line().unwrap_or_default())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::DateTime;
    use spectral::prelude::*;
    use tempfile::TempDir;

    fn make_timelog(lines: &Vec<String>) -> (TempDir, String) {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("timelog.txt");
        let filename = path.to_str().unwrap();
        let file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(filename)
            .unwrap();
        let mut stream = BufWriter::new(file);
        lines
            .iter()
            .for_each(|line| writeln!(&mut stream, "{}", line).unwrap());
        stream.flush().unwrap();
        (tmpdir, filename.to_owned())
    }

    fn touch_timelog() -> (TempDir, String) {
        make_timelog(&vec![String::new()])
    }

    #[test]
    fn test_new() {
        let logfile = Logfile::new("./foo.txt").unwrap();
        let expected = canonicalize(".")
            .map(|mut pb| {
                pb.push("foo.txt");
                pb.to_str().unwrap().to_owned()
            })
            .unwrap_or("".to_owned());
        assert_that!(logfile.clone_file()).is_equal_to(&expected);
    }

    #[test]
    fn test_new_empty_name() {
        assert_that!(Logfile::new(""))
            .is_err()
            .is_equal_to(&PathError::FilenameMissing);
    }

    #[test]
    fn test_new_bad_path() {
        assert_that!(Logfile::new("./xyzzy/foo.txt")).is_err_containing(PathError::InvalidPath(
            "./xyzzy/foo.txt".to_string(),
            "No such file or directory (os error 2)".to_string(),
        ));
    }

    #[test]
    fn test_exists_false() {
        let logfile = Logfile::new("./foo.txt").unwrap();
        assert_that!(logfile.exists()).is_false();
    }

    #[test]
    fn test_exists_true() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.exists()).is_true();
    }

    #[test]
    fn test_add_line() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.add_line("2021-11-18 18:00:00 +project @task")).is_ok();
        assert_that!(logfile.last_line())
            .contains_value(&String::from("2021-11-18 18:00:00 +project @task"));
    }

    #[test]
    fn test_add_event() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        let event = Event::new(
            "+project @task",
            DateTime::try_from("2021-11-18 18:00:00").expect("Bad date"),
        );
        assert_that!(logfile.add_event(&event)).is_ok();
        assert_that!(logfile.last_line())
            .contains_value(&String::from("2021-11-18 18:00:00 +project @task"));
    }

    #[test]
    fn test_last_line_missing() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_line()).contains_value(String::new());
    }

    #[test]
    fn test_last_line_empty() {
        let (_tmpdir, filename) = make_timelog(&vec![]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_line()).is_none();
    }

    #[test]
    fn test_last_line_lines() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_owned(),
            "2021-11-18 17:04:02 +bar".to_owned(),
            "2021-11-18 17:08:04 +baz".to_owned(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_line()).contains_value(&"2021-11-18 17:08:04 +baz".to_owned());
    }

    #[test]
    fn test_last_event() {
        let (_tmpdir, filename) = make_timelog(&vec![]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_event())
            .is_err()
            .is_equal_to(&Error::InvalidEventLine);
    }

    #[test]
    fn test_last_event_lines() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_owned(),
            "2021-11-18 17:04:02 +bar".to_owned(),
            "2021-11-18 17:08:04 +baz".to_owned(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let expected = Event::from_line("2021-11-18 17:08:04 +baz").unwrap();
        assert_that!(logfile.last_event())
            .is_ok()
            .is_equal_to(&expected);
    }
}
