//! Interface to the stack file for the timelog application.
//!
//! # Examples
//!
//! ```rust
//! use timelog::stack::Stack;
//! # fn main() -> Result<(), timelog::Error> {
//! let stack = Stack::new("./stack.txt" )?;
//!
//! stack.push("+Project @Task More detail");
//! let task = stack.pop().expect("Can't pop task");
//! println!("{:?}", task);
//! stack.clear();
//! #   Ok(())
//! # }
//! ```

use std::fs::{canonicalize, remove_file, File, OpenOptions};
use std::io::prelude::*;
use std::io::{BufRead, BufReader, BufWriter, Seek};
use std::path::{Path, PathBuf};

#[doc(inline)]
use crate::error::{Error, PathError};
use crate::Result;

/// Represent the stack file on disk.
#[derive(Debug)]
pub struct Stack(String);

impl Stack {
    /// Creates a [`Stack`] object wrapping the supplied file.
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
    /// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
    pub fn new(file: &str) -> std::result::Result<Self, PathError> {
        if file.is_empty() {
            return Err(PathError::FilenameMissing);
        }
        let mut dir = PathBuf::from(file);
        let filename = dir
            .file_name()
            .ok_or(PathError::FilenameMissing)?
            .to_os_string();
        dir.pop();

        let mut candir = canonicalize(dir)
            .map_err(|e| PathError::InvalidPath(file.to_owned(), e.to_string()))?;
        candir.push(filename);
        Ok(Self(candir.to_str().unwrap().to_owned()))
    }

    /// Open the stack file, return a [`File`].
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if unable to open the file.
    pub fn open(&self) -> std::result::Result<File, PathError> {
        File::open(&self.0).map_err(|e| PathError::FileAccess(self.0.to_owned(), e.to_string()))
    }

    // Clone the filename
    fn clone_file(&self) -> String {
        self.0.to_owned()
    }

    /// Return `true` if the timelog file exists
    pub fn exists(&self) -> bool {
        Path::new(&self.0).exists()
    }

    /// Truncates the stack file, removing all items from the stack.
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file is not accessible.
    pub fn clear(&self) -> std::result::Result<(), PathError> {
        remove_file(&self.0).map_err(|e| PathError::FileAccess(self.clone_file(), e.to_string()))
    }

    /// Adds a new event to the stack file.
    ///
    /// ## Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the stack file cannot be written.
    pub fn push(&self, task: &str) -> std::result::Result<(), PathError> {
        let file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(&self.0)
            .map_err(|e| PathError::FileAccess(self.clone_file(), e.to_string()))?;
        let mut stream = BufWriter::new(file);
        writeln!(&mut stream, "{}", task)
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        Ok(())
    }

    /// Remove the most recent task from the stack file and return the task string.
    pub fn pop(&self) -> Option<String> {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&self.0)
            .ok()?;
        let (line, pos) = self.find_last_line(&mut file)?;

        file.set_len(pos).ok()?;
        Some(line)
    }

    /// Remove one or more tasks from the stack file.
    ///
    /// If `arg` is 0, remove one item.
    /// If `arg` is a positive number, remove that many items from the stack.
    ///
    /// ## Errors
    ///
    /// - Return [`Error::StackPop`] if attempts to pop more items than exist in the stack file.
    pub fn drop(&self, arg: u32) -> Result<()> {
        (0..std::cmp::max(1,arg))
            .try_for_each(|_| self.pop().map(|_| ()))
            .ok_or(Error::StackPop)
    }

    // Find the last line in the stack file.
    fn find_last_line(&self, file: &mut File) -> Option<(String, u64)> {
        let mut last_pos = 0u64;
        let mut last_line = String::new();

        if file.metadata().ok()?.len() == 0u64 {
            return None;
        }

        let mut reader = BufReader::new(file);
        while let Some((line, pos)) = self.last_line_and_pos(&mut reader) {
            last_pos = pos;
            last_line = line.trim_end().to_owned();
        }

        Some((last_line, last_pos))
    }

    /// Find the last line and the position of the beginning of that line in the stack file.
    fn last_line_and_pos<R: BufRead + Seek>(&self, reader: &mut R) -> Option<(String, u64)> {
        let pos = reader.stream_position().ok()?;
        let mut line = String::new();
        if 0 == reader.read_line(&mut line).ok()? {
            return None;
        }
        Some((line, pos))
    }

    /// Format the stack as a [`String`].
    ///
    /// The stack will be formatted such that the most recent item is listed
    /// first.
    pub fn list(&self) -> String {
        let file = match File::open(&self.0) {
            Ok(f) => f,
            Err(_) => return String::new(),
        };
        let lines: Vec<String> = BufReader::new(file).lines().map(|rl| rl.unwrap()).collect();
        let output = String::new();
        lines.iter().rev().fold(output, |mut acc, l| {
            acc.push_str(l);
            acc.push('\n');
            acc
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;
    use tempfile::TempDir;

    use std::path::Path;

    #[test]
    fn test_new_missing_file() {
        assert_that!(Stack::new("")).is_err_containing(PathError::FilenameMissing);
    }

    #[test]
    fn test_new_bad_path() {
        let mut stackdir = TempDir::new()
            .expect("Cannot make tempdir")
            .path()
            .to_path_buf();
        stackdir.push("foo");
        stackdir.push("stack.txt");

        let file = stackdir.as_path().to_str().unwrap();
        assert_that!(Stack::new(file)).is_err_containing(PathError::InvalidPath(
            file.to_owned(),
            "No such file or directory (os error 2)".to_string(),
        ));
    }

    #[test]
    fn test_new() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        assert_that!(Stack::new(filename)).is_ok();
    }

    #[test]
    fn test_push() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(Path::new(filename).exists()).is_false();

        let task = "+house @todo change filters";
        assert_that!(stack.push(task)).is_ok();
        let path = Path::new(filename);
        assert_that!(path.is_file()).is_true();

        // test file length and handle newline lengths
        let filelen = path.metadata().expect("metadata fail").len() as usize;
        assert_that!(filelen).is_equal_to(task.len() + 1);

        assert_that!(stack.push(task)).is_ok();
        let filelen = path.metadata().expect("metadata fail").len() as usize;
        assert_that!(filelen).is_equal_to(2 * task.len() + 2);
    }

    #[test]
    fn test_pop() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.pop()).contains(String::from("+home @todo second"));
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));

        let path = Path::new(filename);
        assert_that!(path.is_file()).is_true();
        // test file length and handle newline lengths
        let filelen = path.metadata().expect("metadata fail").len() as usize;
        assert_that!(filelen).is_equal_to(0);
    }

    #[test]
    fn test_clear() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.clear()).is_ok();
        assert_that!(Path::new(filename).exists()).is_false();
    }

    #[test]
    fn test_drop_0() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.drop(0)).is_ok();
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));
    }

    #[test]
    fn test_drop_1() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.drop(1)).is_ok();
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));
    }

    #[test]
    fn test_drop_2() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n+home @todo third\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.drop(2)).is_ok();
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));
    }

    #[test]
    fn test_list() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n+home @todo third\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.list()).is_equal_to(String::from(
            "+home @todo third\n+home @todo second\n+home @todo first\n",
        ));
    }
}
