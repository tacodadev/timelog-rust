//! Represention of a task event.
//!
//! # Examples
//!
//! ```rust
//! use std::time::Duration;
//! use timelog::{DateTime, TaskEvent};
//! 
//! # fn main() {
//! let timestamp = DateTime::new((2022, 03, 14), (10, 0, 0)).expect("Invalid date or time");
//! let mut event = TaskEvent::new(
//!     timestamp,
//!     Some("project_1".into()),
//!     Duration::from_secs(300)
//!     );
//! println!("{}", event.project());
//! event.add_dur(Duration::from_secs(3000));
//! println!("{:?}", event.duration());
//! # }
//! ```
//!
//! # Description
//!
//! The [`TaskEvent`] type represents a single task event containing a
//! start time and duration.

#[doc(inline)]
use crate::date::DateTime;

use std::time::Duration;

/// Structure representing a single task event.
#[derive(Debug, Clone, Eq, Ord, PartialOrd, PartialEq)]
pub struct TaskEvent {
    /// Timestamp of the start of this event
    start: DateTime,
    /// Optional [`String`] representing the project
    proj: Option<String>,
    /// Calculated total [`Duration`] of the event
    dur: Duration,
}

impl TaskEvent {
    /// Create a [`TaskEvent`] with the supplied parameters.
    pub fn new(start: DateTime, proj: Option<String>, dur: Duration) -> Self {
        Self { start, proj, dur }
    }

    /// Return a tuple with two [`TaskEvent`]s, split at the supplied [`Duration`].
    /// The first starts at the same starting point as this [`TaskEvent`] and
    /// stopping at the supplied [`Duration`]. The second starts [`Duration`] after
    /// the start, containg the rest of the time.
    pub fn split(&self, dur: Duration) -> Option<(Self, Self)> {
        Some((
            Self {
                start: self.start.clone(),
                proj: self.proj(),
                dur: dur
            },
            Self {
                start: self.start.add(dur).ok()?,
                proj: self.proj(),
                dur: self.dur - dur
            }
        ))
    }

    /// Return the project as a String if one exists.
    pub fn proj(&self) -> Option<String> {
        self.proj.as_ref().map(|p| p.to_string())
    }

    /// Return the project as a String defaulting to an empty string.
    pub fn project(&self) -> String {
        self.proj().unwrap_or(String::new())
    }

    /// Return the hour of the start time.
    pub fn hour(&self) -> usize {
        self.start.hour() as usize
    }

    /// Return the starting [`DateTime`].
    pub fn start(&self) -> &DateTime {
        &self.start
    }

    /// Extend the [`TaskEvent`]'s duration by the supplied amount.
    pub fn add_dur(&mut self, dur: Duration) {
        self.dur += dur;
    }

    /// Return the [`Duration`] of this [`TaskEvent`].
    pub fn duration(&self) -> Duration {
        self.dur
    }

    /// Return the number of seconds this [`TaskEvent`] has lasted.
    pub fn as_secs(&self) -> u64 {
        self.dur.as_secs()
    }

    /// Return the number of seconds after the hour of the starting time.
    pub fn second_offset(&self) -> u32 {
        self.start.second_offset()
    }
}
